/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "sprite.h"

#include "utility/memory.h"
#include "utility/string.h"
#include "utility/file.h"
#include "utility/path.h"
#include "utility/print.h"
#include "utility/error.h"

#include <stdlib.h>
#include <string.h>

struct RSDKSprite *SpriteNew(void)
{
    return MemCalloc(sizeof(struct RSDKSprite));
}

struct RSDKAnimation *SpriteAddAnimation(struct RSDKSprite *sprite, char *name, int16_t numFrames, bool allocFrames)
{
    int16_t animNum = sprite->numAnimations;
    if (animNum == INT16_MAX)
        ThrowError("Too many animations");

    sprite->numAnimations++;
    sprite->animations = MemRealloc(sprite->animations, sprite->numAnimations * sizeof(*sprite->animations));
    sprite->animations[animNum] = AnimationNew(name, numFrames, sprite->numHitboxes, allocFrames);

    return sprite->animations[animNum];
}

struct RSDKAnimation *SpriteSearchForAnimation(struct RSDKSprite *sprite, const char *check)
{
    for (int16_t i = 0; i < sprite->numAnimations; i++) {
        struct RSDKAnimation *anim = sprite->animations[i];
        if (!strcmp(anim->name, check))
            return anim;
    }

    return NULL;
}

struct RSDKAnimation *AnimationNew(char *name, int16_t numFrames, uint8_t numHitboxes, bool allocFrames)
{
    if (numFrames < 0)
        ThrowAbort("Can't create animation with %d frames", numFrames);

    struct RSDKAnimation *anim = MemAlloc(sizeof(*anim));

    anim->name = name;
    anim->numFrames = numFrames;
    anim->frames = NULL;
    anim->speed = 0;
    anim->loopFrame = 0;
    anim->flags = 0;

    if (numFrames) {
        anim->frames = MemAlloc(numFrames * sizeof(*anim->frames));

        for (int16_t i = 0; i < numFrames; i++)
            anim->frames[i] = allocFrames ? FrameNew(numHitboxes) : NULL;
    }

    return anim;
}

void AnimationInsertFrame(struct RSDKAnimation *anim, struct RSDKFrame *frame, int16_t index)
{
    if (index == -1) {
        for (int16_t i = 0; i < anim->numFrames; i++) {
            if (!anim->frames[i]) {
                anim->frames[i] = frame;
                return;
            }
        }

        // Pass the buck around
    } else if (index < 0 || index > anim->numFrames)
        ThrowError("Can't insert frame into index %d", index);
    else if (index == anim->numFrames) {
        anim->frames = MemRealloc(anim->frames, (anim->numFrames + 1) * sizeof(*anim->frames));
        anim->frames[anim->numFrames] = frame;
        anim->numFrames++;
        return;
    } else if (!anim->frames[index]) {
        anim->frames[index] = frame;
        return;
    }

    anim->numFrames++;
    anim->frames = MemRealloc(anim->frames, anim->numFrames * sizeof(*anim->frames));

    for (int16_t i = anim->numFrames - 1; i > index; i++)
        anim->frames[i] = anim->frames[i - 1];

    anim->frames[index] = frame;
}

void AnimationRemoveFrame(struct RSDKAnimation *anim, int16_t index)
{
    if (!anim->numFrames || index < 0 || index >= anim->numFrames)
        ThrowError("Can't remove frame %d", index);

    anim->numFrames--;

    if (!anim->numFrames) {
        AnimationClear(anim);
        return;
    }

    FrameDelete(anim->frames[index]);

    for (int16_t i = index; i < anim->numFrames; i++)
        anim->frames[i] = anim->frames[i + 1];

    if (anim->loopFrame >= index)
        anim->loopFrame--;

    anim->frames = MemRealloc(anim->frames, anim->numFrames * sizeof(*anim->frames));
}

void AnimationResize(struct RSDKAnimation *anim, int16_t numFrames, uint8_t numHitboxes, bool allocFrames)
{
    if (!numFrames) {
        AnimationClear(anim);
        return;
    } else if (numFrames < 0 || numFrames == INT16_MAX)
        ThrowError("Can't resize animation to %d frames", numFrames);

    bool delete = false;

    if (numFrames == anim->numFrames)
        return;
    else if (numFrames < anim->numFrames) {
        for (int16_t i = numFrames; i < anim->numFrames; i++)  {
            if (anim->frames[i])
                FrameDelete(anim->frames[i]);
        }

        delete = true;
    }

    int16_t start = anim->numFrames, end = numFrames;

    anim->numFrames = numFrames;
    anim->frames = MemRealloc(anim->frames, numFrames * sizeof(*anim->frames));

    if (delete)
        return;

    for (int16_t i = start; i < end; i++)
        anim->frames[i] = allocFrames ? FrameNew(numHitboxes) : NULL;
}

void AnimationClear(struct RSDKAnimation *anim)
{
    for (int16_t i = 0; i < anim->numFrames; i++) {
        if (anim->frames[i])
            FrameDelete(anim->frames[i]);
    }

    free(anim->frames);

    anim->numFrames = 0;
    anim->frames = NULL;
}

void AnimationDelete(struct RSDKAnimation *anim)
{
    AnimationClear(anim);
    free(anim->name);
    free(anim);
}

struct RSDKFrame *FrameNew(uint8_t numHitboxes)
{
    struct RSDKFrame *frame = MemCalloc(sizeof(*frame));

    if (numHitboxes) {
        frame->numHitboxes = numHitboxes;
        frame->hitboxes = MemAlloc(numHitboxes * sizeof(struct RSDKHitbox));

        for (size_t j = 0; j < numHitboxes; j++) {
            struct RSDKHitbox *hitbox = &frame->hitboxes[j];
            hitbox->left = 0;
            hitbox->top = 0;
            hitbox->right = 0;
            hitbox->bottom = 0;
        }
    }

    return frame;
}

void FrameDelete(struct RSDKFrame *frame)
{
    if (frame->numHitboxes)
        free(frame->hitboxes);

    free(frame);
}

uint8_t SpriteAddSheet(struct RSDKSprite *sprite, char *sheet)
{
    uint8_t sheetNum = sprite->numSheets;

    if (sheetNum == UINT8_MAX)
        ThrowError("Too many sheets");

    for (uint8_t i = 0; i < sprite->numSheets; i++) {
        if (!strcmp(sheet, sprite->sheetNames[i]))
            return i;
    }

    sprite->sheetNames = MemRealloc(sprite->sheetNames, ++sprite->numSheets * sizeof(char **));
    sprite->sheetNames[sheetNum] = sheet;

    return sheetNum;
}

void SpriteDelete(struct RSDKSprite *sprite)
{
    for (size_t i = 0; i < sprite->numSheets; i++)
        free(sprite->sheetNames[i]);

    for (size_t i = 0; i < sprite->numHitboxes; i++)
        free(sprite->hitboxNames[i]);

    for (int16_t i = 0; i < sprite->numAnimations; i++) {
        struct RSDKAnimation *anim = sprite->animations[i];

        for (int16_t j = 0; j < anim->numFrames; j++) {
            free(anim->frames[j]->hitboxes);
            free(anim->frames[j]);
        }

        free(anim->frames);
        free(anim->name);
        free(anim);
    }

    free(sprite->sheetNames);
    free(sprite->hitboxNames);

    free(sprite->animations);
    free(sprite);
}
