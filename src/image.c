/*
 * MIT License
 *
 * Copyright (c) 2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "image.h"

#include "utility/memory.h"
#include "utility/path.h"
#include "utility/print.h"
#include "utility/error.h"

#include <stdlib.h>
#include <string.h>

static void PaintBuffer(uint32_t *dest, uint32_t *source, size_t size);
static void ClearBuffer(uint32_t *dest, size_t size);
static uint32_t *PrepareFrame(struct plum_image *image, size_t size);

struct plum_image *ImageLoad(const char *path)
{
    unsigned error;
    struct plum_image *image = plum_load_image(path, PLUM_FILENAME, PLUM_COLOR_32, &error);
    if (!image)
        ThrowError("Couldn't read image %s: %s", path, plum_get_error_text(error));
    if (image->frames == 1)
        return image;

    uint8_t *disposalData = ImageGetDisposalMetadata(image);

    if (disposalData) {
        size_t imgSize = image->width * image->height;
        size_t bufSize = imgSize * 4;
        uint32_t *buffer = PrepareFrame(image, imgSize);

        int disposal = disposalData[0];

        for (size_t i = 1; i < image->frames; i++) {
            if (disposal == PLUM_DISPOSAL_BACKGROUND)
                ClearBuffer(buffer, imgSize);

            uint32_t PLUM_PIXEL_ARRAY(imgData, image) = image->data;
            uint32_t *source = &imgData[i][0][0];

            PaintBuffer(buffer, source, imgSize);
            memcpy(source, buffer, bufSize);

            disposal = disposalData[i];
        }

        free(buffer);
    }

    return image;
}

static void PaintBuffer(uint32_t *dest, uint32_t *source, size_t size)
{
    uint32_t empty = PLUM_COLOR_VALUE_32(0, 0, 0, 255);

    while (size--) {
        uint32_t px = *source++;
        if (px != empty)
            *dest = px;
        dest++;
    }
}

static void ClearBuffer(uint32_t *dest, size_t size)
{
    uint32_t empty = PLUM_COLOR_VALUE_32(0, 0, 0, 255);

    while (size--)
        *dest++ = empty;
}

static uint32_t *PrepareFrame(struct plum_image *image, size_t size)
{
    uint32_t *buffer = MemAlloc(size * 4);
    memcpy(buffer, image->data, size * 4);
    return buffer;
}

uint8_t ImageListAdd(struct ImageList *list, const char *image)
{
    uint8_t sheetNum = list->count;

    if (sheetNum == UINT8_MAX)
        ThrowError("Too many images");

    for (uint8_t i = 0; i < list->count; i++) {
        if (!strcmp(image, list->names[i]))
            return i;
    }

    char *stripped = StripFileNamePath(image);

    list->names[sheetNum] = image;
    list->paths[sheetNum] = ChangeFileNameExtension(stripped, ".png");
    list->images[sheetNum] = ImageLoad(image);
    list->count++;

    free(stripped);

    return sheetNum;
}

static void *ImageGetMetadata(struct plum_image *image, int type)
{
    struct plum_metadata *metadata = image->metadata;

    while (metadata) {
        if (metadata->type == type)
            return metadata->data;

        metadata = metadata->next;
    }

    return NULL;
}

uint64_t *ImageGetDurationMetadata(struct plum_image *image)
{
    return (uint64_t*)ImageGetMetadata(image, PLUM_METADATA_FRAME_DURATION);
}

uint8_t *ImageGetDisposalMetadata(struct plum_image *image)
{
    return (uint8_t*)ImageGetMetadata(image, PLUM_METADATA_FRAME_DISPOSAL);
}

void ImageListFree(struct ImageList *list)
{
    for (uint8_t i = 0; i < list->count; i++) {
        free(list->paths[i]);
        plum_destroy_image(list->images[i]);
    }
}
