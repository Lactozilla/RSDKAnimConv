/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef ARGS_H
#define ARGS_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "utility/list.h"

enum ArgumentType {
    ARG_FLAG,
    ARG_STRING,
    ARG_NUMBER,
    ARG_OPTION,
    ARG_LIST
};

struct OptionDict {
    char *key;
    int value;
};

struct ProgramArgument {
    char *name;
    uint8_t type;

    union {
        char **string;
        bool *flag;
        int *number;
        struct List **list;
    };

    struct {
        struct OptionDict *dict;
        int *target;
    } option;
};

enum ProgramCommand {
    COMMAND_NONE,
    COMMAND_CONVERT,
    COMMAND_CREATE,

    NUM_PROGRAM_COMMANDS
};

extern size_t argCount;
extern char **argList;

void ParseArgs(struct ProgramArgument **supportedArgs);

bool FindArgsForHelp(struct ProgramArgument *progArgList);
bool SearchInHelpArgs(const char *check, int *helpArgOption);
bool HasHelpArgs(void);
void FreeHelpArgs(void);

extern size_t programCommand;
extern const char *programCommandList[];

size_t FindProgramCommand(const char *cmd);
bool SetProgramCommand(const char *cmd);
void ThrowUnknownCommandError(const char *cmd);

extern struct List *inputFileNamesArg;
extern char *outputFileNameArg;

extern struct OptionDict inputFormatDict[];
extern struct OptionDict outputFormatDict[];
extern struct OptionDict jsonExportModeDict[];

#define ARGS_USED(c) ((signed)c - 1)
#define ARG_DASHES "--"

#define COMMON_CMD_ARGS \
    {"input", ARG_LIST, .list = &inputFileNamesArg}, \
    {"output", ARG_STRING, .string = &outputFileNameArg}

#define COMMON_EXPORT_CMD_ARGS \
    {"jsonExportMode", ARG_OPTION, .option = {jsonExportModeDict, &jsonExportMode}}, \
    {"jsonIndentSize", ARG_STRING, .string = &jsonIndentSize}, \
    {"jsonEscapeSlashes", ARG_FLAG, .flag = &jsonEscapeSlashes}

#define END_CMD_ARGS { NULL, 0, .string = NULL }

#endif
