/*
 * MIT License
 *
 * Copyright (c) 2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SHEET_H
#define SHEET_H

#include <stdint.h>
#include <stdbool.h>

#include "sprite.h"
#include "image.h"

#include "formats/rsdk.h"
#include "utility/list.h"

struct SheetFrame {
    uint16_t *x, *y;

    uint16_t srcX, srcY;
    uint16_t w, h;

    uint8_t *sprSheetID;
    uint8_t imageListIndex;
    uint32_t imgFrame;
    bool *merged;

    bool hasBgColor;
    uint8_t bgColor[4];
};

struct SpriteSheet {
    uint16_t width, height;
    char *path;

    size_t numFrames;
    struct SheetFrame **frames;
};

enum SheetSortingMode {
    SHEET_SORT_NONE,
    SHEET_SORT_AREA,
    SHEET_SORT_WIDTH,
    SHEET_SORT_HEIGHT,
    SHEET_SORT_MAXSIDE
};

struct SpriteSheet *SheetNew(int16_t width, int16_t height, char *path);
struct SpriteSheet *SheetCopy(struct SpriteSheet *src);
struct SheetFrame *SheetAddFrame(struct SpriteSheet *sheet, struct RSDKFrame *frame,
    struct ImageList *imageList, uint8_t imageListIndex, uint32_t imgFrame, uint8_t *bgColor,
    struct BoundingBox *findBounds);
void SheetRemoveFrame(struct SpriteSheet *sheet, size_t index);

bool SheetCompareFrames(struct SheetFrame *frameA, struct SheetFrame *frameB, struct ImageList *imageList);

#define MAX_SHEET_GEN 32
#define DEFAULT_SHEET_SIZE 64
#define MAX_SHEET_SIZE 1024

size_t SheetPackFrames(struct RSDKSprite *sprite,
    struct List *sheetList, size_t listSize,
    uint16_t maxWidth, uint16_t maxHeight,
    enum SheetSortingMode sortMode, struct ImageList *imageList,
    const char *sheetPath, const char *sheetFilename, const char *sheetExt);

void SheetDelete(struct SpriteSheet *sheet);

#endif
