/*
 * MIT License
 *
 * Copyright (c) 2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CREATE_H
#define CREATE_H

#include "args.h"
#include "sprite.h"

struct AnimCollection {
    size_t numAnimations;
    struct AnimationSource **animations;
};

struct AnimationFrame {
    struct BoundingBox bounds;

    uint32_t startIndex;
    uint32_t length;
    uint32_t loopIndex;

    bool isCentered;
    int16_t centerX, centerY;

    bool hasBgColor;
    uint8_t bgColor[4];

    char *sheet;
};

struct AnimationSource {
    struct AnimationFrame **frames;
    size_t numFrames;

    bool useLoopIndex;
    uint32_t loopIndex;
    int16_t speed;

    char *name;
};


void CreateSprites(struct List *inputList, const char *outputFn);

extern struct ProgramArgument cmdArgs_create[];
#endif
