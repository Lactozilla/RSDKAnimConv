/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef RSDK_H
#define RSDK_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

struct RSDKSprite {
    uint32_t numFrames;

    uint8_t numSheets;
    char **sheetNames;

    uint8_t numHitboxes;
    char **hitboxNames;

    int16_t numAnimations;
    struct RSDKAnimation **animations;
};

struct RSDKAnimation {
    char *name;

    int16_t numFrames;
    struct RSDKFrame **frames;

    int16_t speed;
    uint8_t loopFrame;
    uint8_t flags;
};

struct RSDKFrame {
    uint8_t sheet;

    uint16_t duration;
    uint16_t unicodeChar; // RSDK-Reverse calls this unicodeChar now
    uint16_t x, y;
    uint16_t width, height;
    int16_t centerX, centerY;

    uint8_t numHitboxes;
    struct RSDKHitbox *hitboxes;
};

struct RSDKHitbox {
    int16_t left;
    int16_t top;
    int16_t right;
    int16_t bottom;
};

struct RSDKSprite *RSDKReadSprite(FILE *f);
void RSDKWriteSprite(struct RSDKSprite *sprite, FILE *f);
bool RSDKCheckSignature(FILE *f);

#endif
