/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "json.h"
#include "rsdk.h"

#include "../utility/parser.h"
#include "../utility/memory.h"
#include "../utility/string.h"
#include "../utility/print.h"
#include "../utility/error.h"

#include "../sprite.h"

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

int jsonExportMode = JSON_EXPORT_HATCH;
bool jsonEscapeSlashes = false;

enum
{
    KEY_SPRITE_VERSION,
    KEY_SPRITE_EXPORTER,

    KEY_SPRITE_SHEETS,
    KEY_SPRITE_HITBOXES,
    KEY_SPRITE_ANIMATIONS,

    KEY_ANIMATION_NAME,
    KEY_ANIMATION_SPEED,
    KEY_ANIMATION_LOOP,
    KEY_ANIMATION_FLAGS,
    KEY_ANIMATION_FRAMES,

    KEY_FRAME_SHEET,
    KEY_FRAME_DURATION,
    KEY_FRAME_ID,
    KEY_FRAME_SOURCE,
    KEY_FRAME_SOURCE_X,
    KEY_FRAME_SOURCE_Y,
    KEY_FRAME_SIZE,
    KEY_FRAME_SIZE_WIDTH,
    KEY_FRAME_SIZE_HEIGHT,
    KEY_FRAME_PIVOT,
    KEY_FRAME_PIVOT_X,
    KEY_FRAME_PIVOT_Y,
    KEY_FRAME_HITBOXES,

    KEY_HITBOX_LEFT,
    KEY_HITBOX_TOP,
    KEY_HITBOX_RIGHT,
    KEY_HITBOX_BOTTOM,

    NUM_EXPORT_KEYS
};

static const char *jsonKeyNames[NUM_JSON_EXPORTS - 1][NUM_EXPORT_KEYS] = {
    // Hatch
    [JSON_EXPORT_HATCH] = {
        // Metadata
        [KEY_SPRITE_VERSION]    = "version",

        // Sprite
        [KEY_SPRITE_SHEETS]     = "spritesheets",
        [KEY_SPRITE_HITBOXES]   = "hitboxes",
        [KEY_SPRITE_ANIMATIONS] = "animations",

        // Animation
        [KEY_ANIMATION_NAME]    = "name",
        [KEY_ANIMATION_SPEED]   = "speed",
        [KEY_ANIMATION_LOOP]    = "loopFrame",
        [KEY_ANIMATION_FLAGS]   = "rotationType",
        [KEY_ANIMATION_FRAMES]  = "frames",

        // Frame
        [KEY_FRAME_SHEET]       = "sheetID",
        [KEY_FRAME_DURATION]    = "duration",
        [KEY_FRAME_ID]          = "unicodeChar",
        [KEY_FRAME_SOURCE]      = "source",
        [KEY_FRAME_SOURCE_X]    = "x",
        [KEY_FRAME_SOURCE_Y]    = "y",
        [KEY_FRAME_SIZE]        = "size",
        [KEY_FRAME_SIZE_WIDTH]  = "width",
        [KEY_FRAME_SIZE_HEIGHT] = "height",
        [KEY_FRAME_PIVOT]       = "pivot",
        [KEY_FRAME_PIVOT_X]     = "x",
        [KEY_FRAME_PIVOT_Y]     = "y",
        [KEY_FRAME_HITBOXES]    = "hitboxes",

        // Hitbox
        [KEY_HITBOX_LEFT]       = "left",
        [KEY_HITBOX_TOP]        = "top",
        [KEY_HITBOX_RIGHT]      = "right",
        [KEY_HITBOX_BOTTOM]     = "bottom",
    },

    // RSDK Animation Editor v1.5.0
    [JSON_EXPORT_RSDKANIMEDITOR] = {
        // Metadata
        [KEY_SPRITE_VERSION]    = "Anim Version",

        // Sprite
        [KEY_SPRITE_SHEETS]     = "Sheets",
        [KEY_SPRITE_HITBOXES]   = "Hitbox Types",
        [KEY_SPRITE_ANIMATIONS] = "Animations",

        // Animation
        [KEY_ANIMATION_NAME]    = "Name",
        [KEY_ANIMATION_SPEED]   = "Animation Speed",
        [KEY_ANIMATION_LOOP]    = "Loop Index",
        [KEY_ANIMATION_FLAGS]   = "Rotation Flags",
        [KEY_ANIMATION_FRAMES]  = "Frames",

        // Frame
        [KEY_FRAME_SHEET]       = "SheetID",
        [KEY_FRAME_DURATION]    = "Duration",
        [KEY_FRAME_ID]          = "ID",
        [KEY_FRAME_SOURCE]      = "Src",
        [KEY_FRAME_SOURCE_X]    = "x",
        [KEY_FRAME_SOURCE_Y]    = "y",
        [KEY_FRAME_SIZE]        = "Size",
        [KEY_FRAME_SIZE_WIDTH]  = "w",
        [KEY_FRAME_SIZE_HEIGHT] = "h",
        [KEY_FRAME_PIVOT]       = "Pivot",
        [KEY_FRAME_PIVOT_X]     = "x",
        [KEY_FRAME_PIVOT_Y]     = "y",
        [KEY_FRAME_HITBOXES]    = "Hitboxes",

        // Hitbox
        [KEY_HITBOX_LEFT]       = "Left",
        [KEY_HITBOX_TOP]        = "Top",
        [KEY_HITBOX_RIGHT]      = "Right",
        [KEY_HITBOX_BOTTOM]     = "Bottom",
    }
};

static const char **keyNames = NULL;

#ifdef JSON_IMPORTER_DEBUG
#define PrintSpriteInfo(fmt, ...) PrintFormatted(fmt, __VA_ARGS__)
#else
#define PrintSpriteInfo(fmt, ...)
#endif

#define JSON_TRACE_MAX 8

static char *jsonTrace[JSON_TRACE_MAX];
static size_t jsonTraceTop = 0;

static size_t indentLevel = 0;
static size_t indentSize = 0;

static void OutputToJSON(char *string, FILE *f)
{
    size_t length = strlen(string);
    size_t written = fwrite(string, sizeof(char), length, f);

    if (written != length)
        ThrowError("Failed to write file properly (wrote %d bytes out of %d)", written, length);
}

static char *writeBuf = NULL;
static size_t writeBufSize = 0;

enum
{
    WT_COMMA = 1<<0,
    WT_STRING = 1<<1,
    WT_NEWLINE = 1<<2,

    WT_INDENT = 1<<3,
    WT_UNINDENT = 1<<4
};

static void WriteText(const char *line, int flags, FILE *f)
{
    if (flags & WT_UNINDENT && indentLevel)
        indentLevel--;

    size_t indentLen = (indentLevel * indentSize);
    size_t length = strlen(line) + indentLen + 1;

    if (flags & WT_NEWLINE)
        length++;
    if (flags & WT_COMMA)
        length++;
    if (flags & WT_STRING)
        length += 2;

    if (length > writeBufSize) {
        writeBufSize = length;
        writeBuf = MemRealloc(writeBuf, writeBufSize);
    }

    memset(writeBuf, 0x00, writeBufSize);

    if (indentLen)
        memset(writeBuf, ' ', indentLen);

    char *start = writeBuf + indentLen;

    if (flags & WT_STRING) {
        start[0] = '"';
        start++;
    }

    strcat(start, line);

    if (flags & WT_STRING)
        strcat(writeBuf, "\"");
    if (flags & WT_COMMA)
        strcat(writeBuf, ",");
    if (flags & WT_NEWLINE)
        strcat(writeBuf, "\n");

    OutputToJSON(writeBuf, f);

    if (flags & WT_INDENT)
        indentLevel++;
}

static void WriteFormatted(FILE *f, int flags, const char *format, ...)
{
    char buffer[VARIADIC_BUF_SIZE];

    VARIADIC_FORMAT(buffer, format);

    WriteText(buffer, flags, f);
}

static void WriteFormattedLine(FILE *f, int comma, const char *format, ...)
{
    char buffer[VARIADIC_BUF_SIZE];

    VARIADIC_FORMAT(buffer, format);

    int flags = WT_NEWLINE;

    if (comma)
        flags |= WT_COMMA;

    WriteText(buffer, flags, f);
}

static void WriteAndIndent(const char *line, FILE *f)
{
    WriteText(line, WT_NEWLINE, f);
    indentLevel++;
}

#define IS_UTF8(chr) (((chr) & 0xC0) != 0x80)

static const char *WriteUTF8Sequence(char *input, size_t *i)
{
    static const uint32_t offsets[6] = {
        0x00000000UL, 0x00003080UL, 0x000E2080UL,
        0x03C82080UL, 0xFA082080UL, 0x82082080UL
    };

    uint32_t wide = 0, size = 0;

    do {
        wide <<= 6;
        wide += (unsigned char)(input[(*i)++]);
        size++;
    } while (!IS_UTF8(input[*i]));
    wide -= offsets[size - 1];

    static char fmt[7];

    snprintf(fmt, sizeof fmt, "\\u%.4hX", (unsigned short)wide);

    return fmt;
}

static char *escapeSequenceBuf = NULL;
static size_t escapeSequenceBufSize = 0;

static char *InsertEscapeSequences(char *input)
{
    size_t inputLen = strlen(input) + 1;

    if (escapeSequenceBuf == NULL) {
        escapeSequenceBufSize = inputLen;
        escapeSequenceBuf = MemAlloc(escapeSequenceBufSize);
    }

    for (size_t i = 0, j = 0; i < inputLen;) {
        unsigned char c = input[i];

#define CHECK_BUF_SIZE(add) \
        if (j == escapeSequenceBufSize) { \
            escapeSequenceBufSize += add; \
            escapeSequenceBuf = MemRealloc(escapeSequenceBuf, escapeSequenceBufSize); \
        }

#define WRITE_SEQ(chr) \
        CHECK_BUF_SIZE(16); \
        escapeSequenceBuf[j++] = '\\'; \
        escapeSequenceBuf[j++] = chr; \
        i++;

#define WRITE_CHAR(chr) { \
        CHECK_BUF_SIZE(16); \
        escapeSequenceBuf[j++] = chr; \
        i++; \
    }

        switch (c) {
            case '\b': WRITE_SEQ('b'); break;
            case '\f': WRITE_SEQ('f'); break;
            case '\n': WRITE_SEQ('n'); break;
            case '\r': WRITE_SEQ('r'); break;
            case '\t': WRITE_SEQ('t'); break;
            case '\\': WRITE_SEQ('\\'); break;
            case '\"': WRITE_SEQ('\"'); break;
            case '/':
                if (jsonEscapeSlashes) {
                    WRITE_SEQ('/');
                    break;
                } else {
                    WRITE_CHAR(c);
                    break;
                }
            default:
                if (c > 0 && c < 32)
                    ThrowError("Invalid character %d in string", c);
                else if (c >= 127) {
                    // Check if this is the start of an UTF-8 sequence
                    if (!IS_UTF8(c))
                        ThrowError("Invalid character %d in string", c);

                    const char *seq = WriteUTF8Sequence(input, &i);

                    size_t sz = strlen(seq);
                    CHECK_BUF_SIZE(sz + 1);

                    memcpy(&escapeSequenceBuf[j], seq, sz);
                    j += sz;
                } else
                    WRITE_CHAR(c);
        }
    }

    return escapeSequenceBuf;
}

#undef IS_UTF8
#undef WRITE_SEQ

static void WriteStringList(char **list, size_t listSize, FILE *f)
{
    int baseFlags = (WT_STRING | WT_NEWLINE);

    for (size_t i = 0; i < listSize; i++) {
        char *escaped = InsertEscapeSequences(list[i]);

        if (i < listSize-1)
            WriteText(escaped, (baseFlags | WT_COMMA), f);
        else
            WriteText(escaped, baseFlags, f);
    }
}

#define WRITE_STRING(key, value, comma) \
    WriteFormattedLine(f, comma, "\"%s\": \"%s\"", keyNames[key], InsertEscapeSequences(value))

#define WRITE_NUMBER(key, value, comma) WriteFormattedLine(f, comma, "\"%s\": %d", keyNames[key], value)

#define WRITE_NUMBER_OPTIONAL(key, value, comma) \
    if (value) \
        WriteFormattedLine(f, comma, "\"%s\": %d", keyNames[key], value)

static void WriteHitbox(struct RSDKSprite *sprite, struct RSDKHitbox *hitbox, FILE *f)
{
    (void)sprite;

    WRITE_NUMBER_OPTIONAL(KEY_HITBOX_LEFT, hitbox->left, 1);
    WRITE_NUMBER_OPTIONAL(KEY_HITBOX_TOP, hitbox->top, 1);
    WRITE_NUMBER_OPTIONAL(KEY_HITBOX_RIGHT, hitbox->right, 1);
    WRITE_NUMBER_OPTIONAL(KEY_HITBOX_BOTTOM, hitbox->bottom, 0);
}

#define START_ARRAY(key) WriteFormatted(f, (WT_INDENT | WT_NEWLINE), "\"%s\": [", keyNames[key])
#define START_OBJECT(key) WriteFormatted(f, (WT_INDENT | WT_NEWLINE), "\"%s\": {", keyNames[key])

#define END_ARRAY() WriteFormatted(f, (WT_UNINDENT | WT_NEWLINE | WT_COMMA), "]")
#define END_OBJECT() WriteFormatted(f, (WT_UNINDENT | WT_NEWLINE | WT_COMMA), "}")

#define END_LAST_ARRAY() WriteFormatted(f, (WT_UNINDENT | WT_NEWLINE), "]")
#define END_LAST_OBJECT() WriteFormatted(f, (WT_UNINDENT | WT_NEWLINE), "}")

#define WRITE_OBJECTS(func, arr, size) { \
    for (size_t i = 0; i < (unsigned)size; i++) { \
        WriteAndIndent("{", f); \
        func(sprite, &arr[i], f); \
 \
        int flags = (WT_NEWLINE | WT_UNINDENT); \
        if (i < (unsigned)size - 1) \
            flags |= WT_COMMA; \
 \
        WriteFormatted(f, flags, "}"); \
    } \
}

#define WRITE_OBJECTS_FROM_POINTERS(func, arr, size) { \
    for (size_t i = 0; i < (unsigned)size; i++) { \
        WriteAndIndent("{", f); \
        func(sprite, arr[i], f); \
 \
        int flags = (WT_NEWLINE | WT_UNINDENT); \
        if (i < (unsigned)size - 1) \
            flags |= WT_COMMA; \
 \
        WriteFormatted(f, flags, "}"); \
    } \
}

static void WriteFrame(struct RSDKSprite *sprite, struct RSDKFrame *frame, FILE *f)
{
    if (!frame)
        ThrowAbort("JSON exporter: frame is NULL");

    WRITE_NUMBER(KEY_FRAME_SHEET, frame->sheet, 1);
    WRITE_NUMBER_OPTIONAL(KEY_FRAME_DURATION, frame->duration, 1);
    WRITE_NUMBER(KEY_FRAME_ID, frame->unicodeChar, 1);

    if (frame->x && frame->y) {
        START_OBJECT(KEY_FRAME_SOURCE);
            WRITE_NUMBER_OPTIONAL(KEY_FRAME_SOURCE_X, frame->x, 1);
            WRITE_NUMBER_OPTIONAL(KEY_FRAME_SOURCE_Y, frame->y, 0);
        END_OBJECT();
    }

    START_OBJECT(KEY_FRAME_SIZE);
        WRITE_NUMBER(KEY_FRAME_SIZE_WIDTH, frame->width, 1);
        WRITE_NUMBER(KEY_FRAME_SIZE_HEIGHT, frame->height, 0);

    bool hasPivot = frame->centerX && frame->centerY;

    if (hasPivot) {
        END_OBJECT();

        START_OBJECT(KEY_FRAME_PIVOT);
            WRITE_NUMBER_OPTIONAL(KEY_FRAME_PIVOT_X, frame->centerX, 1);
            WRITE_NUMBER_OPTIONAL(KEY_FRAME_PIVOT_Y, frame->centerY, 0);
    } else if (!frame->numHitboxes) {
        END_LAST_OBJECT();
        return;
    }

    if (frame->numHitboxes) {
        if (hasPivot)
            END_OBJECT();

        // Write hitboxes
        START_ARRAY(KEY_FRAME_HITBOXES);
        WRITE_OBJECTS(WriteHitbox, frame->hitboxes, frame->numHitboxes);
        END_LAST_ARRAY();
    } else if (hasPivot)
        END_LAST_OBJECT();
}

static void WriteAnimation(struct RSDKSprite *sprite, struct RSDKAnimation *anim, FILE *f)
{
    if (!anim)
        ThrowAbort("JSON exporter: animation is NULL");

    WRITE_STRING(KEY_ANIMATION_NAME, anim->name, 1);
    WRITE_NUMBER_OPTIONAL(KEY_ANIMATION_SPEED, anim->speed, 1);
    WRITE_NUMBER_OPTIONAL(KEY_ANIMATION_LOOP, anim->loopFrame, 1);
    WRITE_NUMBER_OPTIONAL(KEY_ANIMATION_FLAGS, anim->flags, 1);

#ifdef DEBUG
    if (anim->numFrames < 0)
        abort();
#endif

    // Write frames
    START_ARRAY(KEY_ANIMATION_FRAMES);
    WRITE_OBJECTS_FROM_POINTERS(WriteFrame, anim->frames, anim->numFrames);
    END_LAST_ARRAY();
}

void JSONWriteSprite(struct RSDKSprite *sprite, uint8_t indent, FILE *f)
{
    if (jsonExportMode == JSON_EXPORT_RETROED2)
        jsonExportMode = JSON_EXPORT_RSDKANIMEDITOR;
    else if (jsonExportMode < 0 || jsonExportMode >= NUM_JSON_EXPORTS)
        ThrowError("Invalid JSON export mode %d", jsonExportMode);

    keyNames = jsonKeyNames[jsonExportMode];
    indentSize = indent;

    // Start
    WriteAndIndent("{", f);

    // Write metadata
    if (jsonExportMode == JSON_EXPORT_HATCH) {
        WRITE_STRING(KEY_SPRITE_VERSION, "hatch_v1", 1);
        WriteFormattedLine(f, 1, "\"%s\": \"%s\"", "exporter", "RSDKAnimConv");
    }
    else
        WRITE_NUMBER(KEY_SPRITE_VERSION, 5, 1); // Only exports RSDKv5

    // Write spritesheet
    if (sprite->numSheets) {
        START_ARRAY(KEY_SPRITE_SHEETS);
        WriteStringList(sprite->sheetNames, sprite->numSheets, f);
        END_ARRAY();
    }

    // Write hitbox names
    if (sprite->numHitboxes) {
        START_ARRAY(KEY_SPRITE_HITBOXES);
        WriteStringList(sprite->hitboxNames, sprite->numHitboxes, f);
        END_ARRAY();
    }

    // Write animations
#ifdef DEBUG
    if (sprite->numAnimations < 0)
        abort();
#endif

    if (sprite->numAnimations) {
        START_ARRAY(KEY_SPRITE_ANIMATIONS);
        WRITE_OBJECTS_FROM_POINTERS(WriteAnimation, sprite->animations, sprite->numAnimations);
        END_LAST_ARRAY();
    }

    // Finish
    END_LAST_OBJECT();

    free(escapeSequenceBuf);
    escapeSequenceBuf = NULL;
    escapeSequenceBufSize = 0;

    free(writeBuf);
    writeBuf = NULL;
    writeBufSize = 0;
}

#undef WRITE_NUMBER
#undef WRITE_STRING
#undef WRITE_OBJECTS
#undef WRITE_OBJECTS_FROM_POINTERS
#undef WRITE_NUMBER_OPTIONAL
#undef START_ARRAY

static const char *JSONTypeName(struct JSONValue *value)
{
    const char *typeNames[] = {
        [JSON_UNKNOWN] = "unknown",
        [JSON_STRING] = "string",
        [JSON_NUMBER] = "number",
        [JSON_TRUE] = "true",
        [JSON_FALSE] = "false",
        [JSON_NULL] = "null",
        [JSON_ARRAY] = "array",
        [JSON_OBJECT] = "object"
    };

    return typeNames[value->type];
}

static void JSONTraceEnter(const char *format, ...)
{
    #define TRACE_PREFIX "... "

    char buffer[VARIADIC_BUF_SIZE];
    VARIADIC_FORMAT(buffer, format);

    char trace[VARIADIC_BUF_SIZE + sizeof TRACE_PREFIX];
    snprintf(trace, sizeof trace, TRACE_PREFIX "%s", buffer);

    #undef TRACE_PREFIX

    if (jsonTraceTop == JSON_TRACE_MAX) {
        free(jsonTrace[--jsonTraceTop]);
        free(jsonTrace[--jsonTraceTop]);

        jsonTrace[jsonTraceTop++] = StringCopy("[...]");
    }

    jsonTrace[jsonTraceTop++] = StringCopy(trace);
}

static void JSONTraceExit(void)
{
    if (jsonTraceTop)
        free(jsonTrace[--jsonTraceTop]);
}

static void JSONShowTrace(bool error)
{
    if (!jsonTraceTop)
        return;

    for (size_t i = jsonTraceTop; i > 0; i--) {
        const char *fmt = "       %s\n";

        if (error)
            PrintError(fmt, jsonTrace[i - 1]);
        else
            printf(fmt, jsonTrace[i - 1]);
    }
}

static void JSONReadWarning(const char *format, ...)
{
    char buffer[VARIADIC_BUF_SIZE];
    VARIADIC_FORMAT(buffer, format);

    printf("Warning: %s\n", buffer);

    JSONShowTrace(false);
}

static void JSONReadError(const char *format, ...)
{
    char buffer[VARIADIC_BUF_SIZE];
    VARIADIC_FORMAT(buffer, format);

    PrintError("Error: %s\n", buffer);
    JSONShowTrace(true);

    exit(EXIT_FAILURE);
}

static struct RSDKAnimation *ReadAnimation(struct RSDKSprite *sprite, struct JSONObject *obj);
static struct RSDKFrame *ReadFrame(struct RSDKSprite *sprite, struct JSONObject *obj);
static void ReadHitbox(struct RSDKHitbox *hitbox, struct JSONObject *obj);

#define GET_STRING(field, key) \
    struct JSONValue *field = JSONObjectFind(obj, keyNames[key]); \
    if (!field) \
        JSONReadError("Missing \"" #field "\" property"); \
    else if (field->type != JSON_STRING) \
        JSONReadError("Invalid \"" #field "\" property: expected string, but got %s", JSONTypeName(field)); \
    string = field->u.string;

#define GET_NUMBER(field, key) \
    struct JSONValue *field = JSONObjectFind(obj, keyNames[key]); \
    if (!field) \
        JSONReadError("Missing \"" #field "\" property"); \
    else if (field->type != JSON_NUMBER) \
        JSONReadError("Invalid \"" #field "\" property: expected number, but got %s", JSONTypeName(field)); \
    number = field->u.number;

#define GET_NUMBER_OPTIONAL(field, key) \
    struct JSONValue *field = JSONObjectFind(obj, keyNames[key]); \
    if (!field) \
        number = 0; \
    else if (field->type != JSON_NUMBER) \
        JSONReadError("Invalid \"" #field "\" property: expected number, but got %s", JSONTypeName(field)); \
    else \
        number = field->u.number

struct RSDKAnimation *ReadAnimation(struct RSDKSprite *sprite, struct JSONObject *obj)
{
    struct RSDKAnimation *anim = MemAlloc(sizeof(*anim));

    char *string;
    int number;

    // Name
    GET_STRING(name, KEY_ANIMATION_NAME);
    anim->name = StringCopy(string);

    JSONTraceExit();
    JSONTraceEnter("in animation \"%s\"", anim->name);

    // Speed
    GET_NUMBER_OPTIONAL(speed, KEY_ANIMATION_SPEED);
    anim->speed = number;

    // Loop frame
    GET_NUMBER_OPTIONAL(loopFrame, KEY_ANIMATION_LOOP);
    if (number < 0 || number > UINT8_MAX)
        JSONReadError("Invalid loop frame %d");
    anim->loopFrame = number;

    // Rotation type
    GET_NUMBER_OPTIONAL(flags, KEY_ANIMATION_FLAGS);
    if (number < 0 || number > UINT8_MAX)
        JSONReadError("Invalid rotation type %d");
    anim->flags = number;

    // Frames
    struct JSONValue *frames = JSONObjectFind(obj, keyNames[KEY_ANIMATION_FRAMES]);
    if (!frames)
        JSONReadError("Missing \"%s\" property", keyNames[KEY_ANIMATION_FRAMES]);
    else if (frames->type != JSON_ARRAY) {
        JSONReadError("Invalid \"%s\" property: expected array, but got %s",
            keyNames[KEY_ANIMATION_FRAMES], JSONTypeName(frames));
    }

    anim->numFrames = frames->u.array->numValues;
    sprite->numFrames += anim->numFrames;

    if (anim->numFrames) {
        anim->frames = MemAlloc(anim->numFrames * sizeof(*anim->frames));

        JSONTraceExit();
        JSONTraceEnter("of animation \"%s\"", anim->name);

        for (int16_t i = 0; i < anim->numFrames; i++) {
            struct JSONValue *value = &frames->u.array->values[i];

            if (value->type != JSON_OBJECT)
                JSONReadError("Invalid frame: expected object, but got %s", JSONTypeName(value));

            JSONTraceEnter("in frame %d", i + 1);
            anim->frames[i] = ReadFrame(sprite, value->u.object);
            JSONTraceExit();
        }
    } else
        JSONReadWarning("Missing \"%s\" property", keyNames[KEY_ANIMATION_FRAMES]);

    JSONTraceExit();

    return anim;
}

struct RSDKFrame *ReadFrame(struct RSDKSprite *sprite, struct JSONObject *obj)
{
    struct RSDKFrame *frame = MemAlloc(sizeof(*frame));

    int number;

    // Spritesheet
    GET_NUMBER(sheet, KEY_FRAME_SHEET);

    if (number < 0)
        JSONReadError("Invalid spritesheet %d (min. zero)", number);
    else {
        if (!sprite->numSheets)
            frame->sheet = 0;
        else if (number >= sprite->numSheets || number > UINT8_MAX)
            JSONReadError("Invalid spritesheet %d (max. %d)", number);
    }

    frame->sheet = number;

    // Duration
    GET_NUMBER_OPTIONAL(duration, KEY_FRAME_DURATION);

    if (number < INT16_MIN || number > INT16_MAX)
        JSONReadError("Invalid duration %d", number);

    frame->duration = number;

    // ID
    GET_NUMBER(id, KEY_FRAME_ID);

    if (number < INT16_MIN || number > INT16_MAX)
        JSONReadError("Invalid ID %d", number);

    frame->unicodeChar = number;

    // Source
    struct JSONValue *source = JSONObjectFind(obj, keyNames[KEY_FRAME_SOURCE]);
    if (source) {
        if (source->type != JSON_OBJECT) {
            JSONReadError("Invalid \"%s\" property: expected number, but got %s",
                keyNames[KEY_FRAME_SOURCE], JSONTypeName(source));
        }

        // Source X
        struct JSONValue *srcX = JSONObjectFind(source->u.object, keyNames[KEY_FRAME_SOURCE_X]);
        if (!srcX)
            frame->x = 0;
        else if (srcX->type != JSON_NUMBER)
            JSONReadError("Invalid frame X: expected number, but got %s", JSONTypeName(srcX));
        else
            frame->x = srcX->u.number;

        // Source Y
        struct JSONValue *srcY = JSONObjectFind(source->u.object, keyNames[KEY_FRAME_SOURCE_Y]);
        if (!srcY)
            frame->y = 0;
        else if (srcY->type != JSON_NUMBER)
            JSONReadError("Invalid frame Y: expected number, but got %s", JSONTypeName(srcY));
        else
            frame->y = srcY->u.number;
    } else {
        frame->x = 0;
        frame->y = 0;
    }

    // Size
    struct JSONValue *size = JSONObjectFind(obj, keyNames[KEY_FRAME_SIZE]);
    if (!size)
        JSONReadError("Missing \"%s\" property", keyNames[KEY_FRAME_SIZE]);
    else if (size->type != JSON_OBJECT) {
        JSONReadError("Invalid \"%s\" property: expected number, but got %s",
            keyNames[KEY_FRAME_SIZE], JSONTypeName(size));
    } else {
        // Size width
        struct JSONValue *sizeW = JSONObjectFind(size->u.object, keyNames[KEY_FRAME_SIZE_WIDTH]);
        if (!sizeW)
            JSONReadError("Missing frame width");
        else if (sizeW->type != JSON_NUMBER)
            JSONReadError("Invalid frame width: expected number, but got %s", JSONTypeName(sizeW));
        else
            frame->width = sizeW->u.number;

        // Size height
        struct JSONValue *sizeH = JSONObjectFind(size->u.object, keyNames[KEY_FRAME_SIZE_HEIGHT]);
        if (!sizeH)
            JSONReadError("Missing frame height");
        else if (sizeH->type != JSON_NUMBER)
            JSONReadError("Invalid frame height: expected number, but got %s", JSONTypeName(sizeH));
        else
            frame->height = sizeH->u.number;
    }

    // Pivot
    struct JSONValue *pivot = JSONObjectFind(obj, keyNames[KEY_FRAME_PIVOT]);
    if (pivot) {
        if (pivot->type != JSON_OBJECT) {
            JSONReadError("Invalid \"%s\" property: expected number, but got %s",
                keyNames[KEY_FRAME_PIVOT], JSONTypeName(pivot));
        }

        // Pivot X
        struct JSONValue *pivotX = JSONObjectFind(pivot->u.object, keyNames[KEY_FRAME_PIVOT_X]);
        if (!pivotX)
            frame->centerX = 0;
        else if (pivotX->type != JSON_NUMBER)
            JSONReadError("Invalid frame pivot X: expected number, but got %s", JSONTypeName(pivotX));
        else
            frame->centerX = pivotX->u.number;

        // Pivot Y
        struct JSONValue *pivotY = JSONObjectFind(pivot->u.object, keyNames[KEY_FRAME_PIVOT_Y]);
        if (!pivotY)
            frame->centerY = 0;
        else if (pivotY->type != JSON_NUMBER)
            JSONReadError("Invalid frame pivot Y: expected number, but got %s", JSONTypeName(pivotY));
        else
            frame->centerY = pivotY->u.number;
    } else {
        frame->centerX = 0;
        frame->centerY = 0;
    }

    // Hitboxes
    frame->numHitboxes = 0;
    frame->hitboxes = NULL;

    struct JSONValue *hitboxes = JSONObjectFind(obj, keyNames[KEY_FRAME_HITBOXES]);
    if (hitboxes) {
        if (hitboxes->type != JSON_ARRAY) {
            JSONReadError("Invalid \"%s\" property: expected array, but got %s",
                keyNames[KEY_FRAME_SOURCE], JSONTypeName(hitboxes));
        }

        size_t arrHitboxes = hitboxes->u.array->numValues;
        uint8_t numHitboxes = sprite->numHitboxes;

        if (!arrHitboxes && numHitboxes)
            JSONReadWarning("Sprite has %d hitbox(es), but this frame has none", numHitboxes);
        else if (!numHitboxes && arrHitboxes)
            JSONReadWarning("Frame has %d hitbox(es), but the sprite has none", arrHitboxes);
        else if (arrHitboxes > numHitboxes) {
            int diff = (arrHitboxes - numHitboxes);

            if (diff == 1)
                JSONReadWarning("Frame has one more hitbox than the sprite", diff);
            else
                JSONReadWarning("Frame has %d more hitboxes than the sprite", diff);
        } else if (arrHitboxes < numHitboxes) {
            int diff = (numHitboxes - arrHitboxes);

            if (diff == 1)
                JSONReadWarning("Frame has one less hitbox than the sprite");
            else
                JSONReadWarning("Frame has %d less hitboxes than the sprite");
        }

        if (numHitboxes) {
            frame->numHitboxes = numHitboxes;
            frame->hitboxes = MemCalloc(numHitboxes * sizeof(struct RSDKHitbox));

            for (size_t i = 0; i < numHitboxes && i < arrHitboxes; i++) {
                struct JSONValue *value = &hitboxes->u.array->values[i];

                if (value->type != JSON_OBJECT)
                    JSONReadError("Invalid hitbox: expected object, but got %s", JSONTypeName(value));

                JSONTraceEnter("in hitbox \"%s\"", sprite->hitboxNames[i]);
                ReadHitbox(&frame->hitboxes[i], value->u.object);
                JSONTraceExit();
            }
        }
    }

    return frame;
}

#define READ_HITBOX_SIDE(side, e) \
    GET_NUMBER_OPTIONAL(side, e); \
 \
    if (number < INT16_MIN || number > INT16_MAX) \
        JSONReadError("Invalid hitbox " #side " %d", number); \
 \
    hitbox->side = number

void ReadHitbox(struct RSDKHitbox *hitbox, struct JSONObject *obj)
{
    int number;

    READ_HITBOX_SIDE(left, KEY_HITBOX_LEFT);
    READ_HITBOX_SIDE(top, KEY_HITBOX_TOP);
    READ_HITBOX_SIDE(right, KEY_HITBOX_RIGHT);
    READ_HITBOX_SIDE(bottom, KEY_HITBOX_BOTTOM);
}

#undef READ_HITBOX_SIDE

#undef GET_STRING
#undef GET_NUMBER
#undef GET_NUMBER_OPTIONAL

#define READ_STRING_ARRAY(arrName, property, arrNum, arrField) { \
    struct JSONValue *list = JSONObjectFind(rootObj, keyNames[property]); \
    if (list) { \
        if (list->type != JSON_ARRAY) { \
            JSONReadError("Invalid \"%s\" property: expected array, but was %s", \
                keyNames[property], JSONTypeName(list)); \
        } \
 \
        sprite->arrNum = list->u.array->numValues; \
 \
        PrintSpriteInfo("Spritesheets: %d\n", sprite->arrNum); \
 \
        if (sprite->arrNum) { \
            sprite->arrField = MemAlloc(sprite->arrNum * sizeof(char **)); \
 \
            for (size_t i = 0; i < sprite->arrNum; i++) { \
                struct JSONValue *value = &list->u.array->values[i]; \
 \
                if (value->type != JSON_STRING) \
                    JSONReadError("Invalid " arrName ": expected string, but got %s", JSONTypeName(value)); \
 \
                sprite->arrField[i] = StringCopy(value->u.string); \
                PrintSpriteInfo("  %d: %s\n", i, sprite->arrField[i]); \
            } \
        } \
    } \
}

struct RSDKSprite *JSONReadSprite(FILE *f)
{
    struct JSONValue root = JSONParseText(f);
    struct JSONObject *rootObj = root.u.object;

    if (root.type != JSON_OBJECT)
        JSONReadError("JSON sprite not an object");

    // Allocate new sprite
    struct RSDKSprite *sprite = SpriteNew();

    // Detect version
    enum JSONImportMode importerMode = NUM_JSON_IMPORTS;
    struct JSONValue *version = JSONObjectFind(rootObj, "version");

    if (version) {
        if (version->type == JSON_STRING) {
            char *verStr = version->u.string;

            if (!strcmp(verStr, "hatch_v1"))
                importerMode = JSON_IMPORT_HATCH;
            else
                JSONReadError("Version \"%s\" not supported", verStr);

            PrintSpriteInfo("Version: %s\n", verStr);
        } else
            JSONReadError("Invalid \"version\" property: expected string, but got %s", JSONTypeName(version));
    } else {
        struct JSONValue *version = JSONObjectFind(rootObj, "Version");
        if (!version)
            JSONReadError("Missing version property");

        importerMode = JSON_IMPORT_RSDKANIMEDITOR;

        if (version->type == JSON_NUMBER) {
            int verNum = version->u.number;

            if (verNum != 5)
                JSONReadError("Version %d not supported", verNum);

            PrintSpriteInfo("Version: RSDKv%d\n", verNum);
        } else
            JSONReadError("Invalid \"Version\" property: expected number, but got %s", JSONTypeName(version));
    }

    keyNames = jsonKeyNames[importerMode];

    // Read spritesheets
    READ_STRING_ARRAY("spritesheet", KEY_SPRITE_SHEETS, numSheets, sheetNames);

    if (!sprite->numSheets)
        JSONReadWarning("Sprite has no sheets");

    // Read hitboxes
    READ_STRING_ARRAY("hitbox", KEY_SPRITE_HITBOXES, numHitboxes, hitboxNames);

    // Read animations
    struct JSONValue *anims = JSONObjectFind(rootObj, keyNames[KEY_SPRITE_ANIMATIONS]);
    if (anims) {
        if (anims->type != JSON_ARRAY) {
            JSONReadError("Invalid \"%s\" property: expected array, but was %s",
                keyNames[KEY_SPRITE_ANIMATIONS], JSONTypeName(anims));
        }

        sprite->numAnimations = anims->u.array->numValues;

        PrintSpriteInfo("Animations: %d\n", sprite->numAnimations);

        if (sprite->numAnimations) {
            sprite->animations = MemAlloc(sprite->numAnimations * sizeof(*sprite->animations));

            for (int16_t i = 0; i < sprite->numAnimations; i++) {
                struct JSONValue *value = &anims->u.array->values[i];

                if (value->type != JSON_OBJECT)
                    JSONReadError("Invalid animation: expected object, but got %s", JSONTypeName(value));

                JSONTraceEnter("in animation %d", i + 1);

                sprite->animations[i] = ReadAnimation(sprite, value->u.object);
            }
        }
    }

    if (!sprite->numAnimations)
        JSONReadWarning("Sprite has no animations");
    else {
        PrintSpriteInfo("Frames: %d\n", sprite->numFrames);
    }

    JSONFreeValue(&root);

    return sprite;
}

#undef READ_STRING_ARRAY

struct JSONValue *JSONObjectFind(struct JSONObject *obj, const char *keyName)
{
    for (size_t i = 0; i < obj->numValues; i++) {
        struct JSONValue *key = &obj->keys[i];
        struct JSONValue *value = &obj->values[i];

        if (!strcmp(key->u.string, keyName))
            return value;
    }

    return NULL;
}

static void FreeArray(struct JSONArray *array);

static void FreeObject(struct JSONObject *obj)
{
    for (size_t i = 0; i < obj->numValues; i++) {
        struct JSONValue *key = &obj->keys[i];
        struct JSONValue *value = &obj->values[i];

        free(key->u.string);

        if (value->type == JSON_STRING)
            free(value->u.string);
        else if (value->type == JSON_OBJECT)
            FreeObject(value->u.object);
        else if (value->type == JSON_ARRAY)
            FreeArray(value->u.array);
    }

    free(obj->values);
    free(obj->keys);
    free(obj);
}

static void FreeArray(struct JSONArray *array)
{
    for (size_t i = 0; i < array->numValues; i++) {
        struct JSONValue *value = &array->values[i];

        if (value->type == JSON_STRING)
            free(value->u.string);
        else if (value->type == JSON_OBJECT)
            FreeObject(value->u.object);
        else if (value->type == JSON_ARRAY)
            FreeArray(value->u.array);
    }

    free(array->values);
    free(array);
}

void JSONFreeValue(struct JSONValue *val)
{
    if (val->type == JSON_ARRAY)
        FreeArray(val->u.array);
    else if (val->type == JSON_OBJECT)
        FreeObject(val->u.object);
}
