/*
 * MIT License
 *
 * Copyright (c) 2021 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef JSON_H
#define JSON_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

enum JSONValueType
{
    JSON_UNKNOWN,
    JSON_STRING,
    JSON_NUMBER,
    JSON_TRUE,
    JSON_FALSE,
    JSON_NULL,
    JSON_ARRAY,
    JSON_OBJECT
};

struct JSONValue
{
    enum JSONValueType type;

    union {
        char *string;
        int number;
        struct JSONArray *array;
        struct JSONObject *object;
    } u;
};

struct JSONObject
{
    size_t numValues;
    size_t capacity;

    struct JSONValue *keys;
    struct JSONValue *values;
};

struct JSONArray
{
    size_t numValues;
    size_t capacity;

    struct JSONValue *values;
};

struct RSDKSprite *JSONReadSprite(FILE *f);
void JSONWriteSprite(struct RSDKSprite *sprite, uint8_t indent, FILE *f);

struct JSONValue *JSONObjectFind(struct JSONObject *obj, const char *keyName);
void JSONFreeValue(struct JSONValue *frag);

enum JSONImportMode {
    JSON_IMPORT_HATCH,
    JSON_IMPORT_RSDKANIMEDITOR,
    JSON_IMPORT_RETROED2,

    NUM_JSON_IMPORTS
};

enum JSONExportMode {
    JSON_EXPORT_HATCH,
    JSON_EXPORT_RSDKANIMEDITOR,
    JSON_EXPORT_RETROED2,

    NUM_JSON_EXPORTS
};

extern int jsonExportMode;
extern bool jsonEscapeSlashes;

#endif
