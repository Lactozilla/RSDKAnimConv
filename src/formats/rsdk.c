/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "rsdk.h"

#include "../utility/reader.h"
#include "../utility/writer.h"
#include "../utility/memory.h"
#include "../utility/print.h"
#include "../utility/error.h"

#include "../sprite.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>

#define RSDK_SPRITE_SIGNATURE 0x00525053 // "SPR\0", in little endian

static struct RSDKAnimation *ReadAnimation(struct RSDKSprite *sprite, FILE *f);
static struct RSDKFrame *ReadFrame(struct RSDKSprite *sprite, FILE *f);
static void ReadHitbox(struct RSDKHitbox *hitbox, FILE *f);

static char *ReadRSDKString(FILE *f)
{
    size_t length = ReadUint8(f);
    char *string = MemAlloc(length);

    ReadIntoBuffer((uint8_t *)string, length, f);

    // The string is not null-terminated for some reason?
    if (string[length - 1] != '\0') {
        string = MemRealloc(string, length + 1);
        string[length] = '\0';
    }

    return string;
}

static void WriteRSDKString(char *string, FILE *f)
{
    uint8_t length = strlen(string) + 1;

    WriteUint8(length, f);
    WriteFromBuffer((uint8_t *)string, length, f);
}

#ifdef RSDK_READER_DEBUG
#define PrintSpriteInfo(fmt, ...) PrintFormatted(fmt, __VA_ARGS__)
#define PrintSpriteInfoNoFmt(str) puts(str)
#else
#define PrintSpriteInfo(fmt, ...)
#define PrintSpriteInfoNoFmt(str)
#endif

struct RSDKAnimation *ReadAnimation(struct RSDKSprite *sprite, FILE *f)
{
    struct RSDKAnimation *anim = MemAlloc(sizeof(*anim));

    anim->name = ReadRSDKString(f);
    anim->numFrames = ReadInt16(f);
    anim->speed = ReadInt16(f);
    anim->loopFrame = ReadUint8(f);
    anim->flags = ReadUint8(f);

    anim->frames = MemAlloc(anim->numFrames * sizeof(*anim->frames));

    for (int16_t i = 0; i < anim->numFrames; i++)
        anim->frames[i] = ReadFrame(sprite, f);

    return anim;
}

struct RSDKFrame *ReadFrame(struct RSDKSprite *sprite, FILE *f)
{
    struct RSDKFrame *frame = MemAlloc(sizeof(*frame));

    frame->sheet = ReadUint8(f);
    frame->duration = ReadUint16(f);
    frame->unicodeChar = ReadUint16(f);
    frame->x = ReadUint16(f);
    frame->y = ReadUint16(f);
    frame->width = ReadUint16(f);
    frame->height = ReadUint16(f);
    frame->centerX = ReadInt16(f);
    frame->centerY = ReadInt16(f);

    uint8_t numHitboxes = sprite->numHitboxes;

    if (numHitboxes) {
        frame->numHitboxes = numHitboxes;
        frame->hitboxes = MemAlloc(numHitboxes * sizeof(struct RSDKHitbox));

        for (size_t i = 0; i < numHitboxes; i++)
            ReadHitbox(&frame->hitboxes[i], f);
    } else {
        frame->numHitboxes = 0;
        frame->hitboxes = NULL;
    }

    return frame;
}

void ReadHitbox(struct RSDKHitbox *hitbox, FILE *f)
{
    hitbox->left = ReadInt16(f);
    hitbox->top = ReadInt16(f);
    hitbox->right = ReadInt16(f);
    hitbox->bottom = ReadInt16(f);
}

bool RSDKCheckSignature(FILE *f)
{
    return (ReadUint32(f) == RSDK_SPRITE_SIGNATURE);
}

struct RSDKSprite *RSDKReadSprite(FILE *f)
{
    if (!RSDKCheckSignature(f))
        ThrowError("Not an RSDK sprite (signature does not match)");

    struct RSDKSprite *sprite = SpriteNew();

    // Read frame count
    sprite->numFrames = ReadInt32(f);

    PrintSpriteInfo("Frames: %d\n", sprite->numFrames);

    // Read spritesheet count and spritesheet names
    sprite->numSheets = ReadUint8(f);

    PrintSpriteInfo("Spritesheets: %d\n", sprite->numSheets);

    if (sprite->numSheets) {
        sprite->sheetNames = MemAlloc(sprite->numSheets * sizeof(char**));

        for (size_t i = 0; i < sprite->numSheets; i++) {
            sprite->sheetNames[i] = ReadRSDKString(f);
            PrintSpriteInfo("    - %s\n", sprite->sheetNames[i]);
        }
    }

    // Read hitbox count and hitbox names
    sprite->numHitboxes = ReadUint8(f);

    PrintSpriteInfo("Hitboxes: %d\n", sprite->numHitboxes);

    if (sprite->numHitboxes) {
        sprite->hitboxNames = MemAlloc(sprite->numHitboxes * sizeof(char**));

        for (size_t i = 0; i < sprite->numHitboxes; i++) {
            sprite->hitboxNames[i] = ReadRSDKString(f);
            PrintSpriteInfo("    - %s\n", sprite->hitboxNames[i]);
        }
    }

    // Read animation count and animations
    int16_t numAnimations = ReadInt16(f);

    if (numAnimations > 0) {
        sprite->numAnimations = numAnimations;
        sprite->animations = MemAlloc(numAnimations * sizeof(*sprite->animations));

        PrintSpriteInfo("Animations: %d\n", numAnimations);

        for (int16_t i = 0; i < numAnimations; i++) {
            struct RSDKAnimation *anim = ReadAnimation(sprite, f);

            PrintSpriteInfo("    - %d: %s\n", i, anim->name);
            PrintSpriteInfo("        Frame count: %d\n", anim->numFrames);
            PrintSpriteInfo("        Speed:       %d\n", anim->speed);
            PrintSpriteInfo("        Loop frame:  %d\n", anim->loopFrame);
            PrintSpriteInfo("        Flags:       %d\n", anim->flags);

            sprite->animations[i] = anim;
        }
    } else {
        if (numAnimations < 0)
            PrintSpriteInfo("Sprite has invalid animation count %d", numAnimations);
        else {
            PrintSpriteInfoNoFmt("Sprite has no animations");
        }
    }

    return sprite;
}

static void WriteAnimation(struct RSDKAnimation *anim, FILE *f);
static void WriteFrame(struct RSDKFrame *frame, FILE *f);
static void WriteHitbox(struct RSDKHitbox *hitbox, FILE *f);

static int32_t CountFrames(struct RSDKSprite *sprite);

void RSDKWriteSprite(struct RSDKSprite *sprite, FILE *f)
{
    // Write signature
    WriteUint32(RSDK_SPRITE_SIGNATURE, f);

    // Write frame count
    WriteInt32(CountFrames(sprite), f);

    // Write spritesheet count
    WriteUint8(sprite->numSheets, f);

    // Write spritesheet names
    if (sprite->numSheets) {
        for (size_t i = 0; i < sprite->numSheets; i++)
            WriteRSDKString(sprite->sheetNames[i], f);
    }

    // Write hitbox count
    WriteUint8(sprite->numHitboxes, f);

    // Write hitbox names
    if (sprite->numHitboxes) {
        for (size_t i = 0; i < sprite->numHitboxes; i++)
            WriteRSDKString(sprite->hitboxNames[i], f);
    }

    // Write animation count
    WriteInt16(sprite->numAnimations, f);

    // Write animations
    for (int16_t i = 0; i < sprite->numAnimations; i++)
        WriteAnimation(sprite->animations[i], f);
}

void WriteAnimation(struct RSDKAnimation *anim, FILE *f)
{
    if (!anim)
        ThrowAbort("RSDK exporter: animation is NULL");

    WriteRSDKString(anim->name, f);
    WriteInt16(anim->numFrames, f);
    WriteInt16(anim->speed, f);
    WriteUint8(anim->loopFrame, f);
    WriteUint8(anim->flags, f);

    for (int16_t i = 0; i < anim->numFrames; i++)
        WriteFrame(anim->frames[i], f);
}

void WriteFrame(struct RSDKFrame *frame, FILE *f)
{
    if (!frame)
        ThrowAbort("RSDK exporter: frame is NULL");

    WriteUint8(frame->sheet, f);
    WriteUint16(frame->duration, f);
    WriteUint16(frame->unicodeChar, f);
    WriteUint16(frame->x, f);
    WriteUint16(frame->y, f);
    WriteUint16(frame->width, f);
    WriteUint16(frame->height, f);
    WriteInt16(frame->centerX, f);
    WriteInt16(frame->centerY, f);

    for (size_t i = 0; i < frame->numHitboxes; i++)
        WriteHitbox(&frame->hitboxes[i], f);
}

void WriteHitbox(struct RSDKHitbox *hitbox, FILE *f)
{
    WriteInt16(hitbox->left, f);
    WriteInt16(hitbox->top, f);
    WriteInt16(hitbox->right, f);
    WriteInt16(hitbox->bottom, f);
}

int32_t CountFrames(struct RSDKSprite *sprite)
{
    int32_t numFrames = 0;

    for (int16_t i = 0; i < sprite->numAnimations; i++) {
        struct RSDKAnimation *anim = sprite->animations[i];

        if (!anim)
            ThrowAbort("RSDK exporter: animation is NULL");

        numFrames += anim->numFrames;
    }

    return numFrames;
}
