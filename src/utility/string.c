/*
 * MIT License
 *
 * Copyright (c) 2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "string.h"
#include "memory.h"
#include "print.h"
#include "error.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

char *StringCopy(const char *srcString)
{
    if (!srcString)
        ThrowAbort("No string to copy\n");

    size_t size = strlen(srcString) + 1;
    char *newString = MemAlloc(size);

    memcpy(newString, srcString, size);

    return newString;
}

char *StringConcat(const char *stringA, const char *stringB)
{
    if (!stringA || !stringB)
        ThrowAbort("No string to concatenate\n");

    size_t strLenA = strlen(stringA);
    size_t strLenB = strlen(stringB) + 1;

    char *newString = MemAlloc(strLenA + strLenB);
    char *out = newString;

    memcpy(newString, stringA, strLenA);
    newString += strLenA;
    memcpy(newString, stringB, strLenB);

    return out;
}

size_t StringBufCopy(char *dst, const char *src, size_t sz)
{
    char *d = dst;
    const char *s = src;
    size_t n = sz;

    // Copy as many bytes as will fit
    if (n != 0) {
        while (--n != 0) {
            if ((*d++ = *s++) == '\0')
            break;
        }
    }

    // Not enough room in dst, add NUL and traverse rest of src
    if (n == 0) {
        if (sz != 0)
            *d = '\0'; // NUL-terminate dst
        while (*s++)
            ;
    }

    return s - src - 1; // count does not include NUL
}

char *StringIncNum(const char *str, int inc, int width)
{
    int len = strlen(str);
    while (isdigit(str[--len]))
        ;

    len++;

    inc += strtol(str + len, NULL, 10);
    if (inc < 0)
        inc = 0;

    char buf[21];
    int pLen = snprintf(buf, sizeof buf, "%0*d", width, inc);

    char *p = MemAlloc(len + pLen + 1);
    strncpy(p, str, len);
    p[len] = '\0';

    return strcat(p, buf);
}

bool StringOnlyHasSpaces(const char *string)
{
    size_t len = strlen(string);

    for (size_t i = 0; i < len; i++) {
        if (!isspace(string[i]))
            return false;
    }

    return true;
}
