/*
 * MIT License
 *
 * Copyright (c) 2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "list.h"
#include "memory.h"

struct List *ListNew(size_t capacity)
{
    struct List *list = MemAlloc(sizeof(*list));

#ifdef DEBUG
    if (!capacity)
        abort();
#endif

    list->initialCapacity = capacity;
    list->capacity = capacity;
    list->items = MemAlloc(capacity * sizeof(void *));
    list->size = 0;

    return list;
}

void ListAdd(struct List *list, void *element)
{
    if (list->size == list->capacity) {
        list->capacity += list->initialCapacity;
        list->items = MemRealloc(list->items, list->capacity * sizeof(void *));
    }

    list->items[list->size] = element;
    list->size++;
}

void ListRemove(struct List *list, size_t index)
{
    if (index >= list->size)
        return;

    list->size--;

    for (size_t i = index; i < list->size; i++)
        list->items[i] = list->items[i + 1];
}

void *ListGet(struct List *list, size_t index)
{
    if (index >= list->size)
        return NULL;

    return list->items[index];
}

size_t ListSize(struct List *list)
{
    return list->size;
}

void ListDelete(struct List *list)
{
    free(list->items);
    free(list);
}
