/*
 * MIT License
 *
 * Copyright (c) 2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "csv.h"

#include "memory.h"

#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#include <errno.h>

static ssize_t getline(char **string, size_t *n, FILE *f) {
    if (string == NULL || f == NULL || n == NULL) {
        errno = EINVAL;
        return -1;
    }

    int c = getc(f);
    if (c == EOF)
        return -1;

    size_t pos = 0;
    size_t baseSize = 128;

    if (*string == NULL) {
        *string = MemAlloc(baseSize);
        *n = baseSize;
    }

    for (; c != EOF; c = getc(f)) {
        if (pos + 1 >= *n) {
            size_t size = max(*n + (*n >> 2), baseSize);
            *string = MemRealloc(*string, size);
            *n = size;
        }

        ((unsigned char *)(*string))[pos++] = c;
        if (c == '\n')
            break;
    }

    (*string)[pos] = '\0';
    return pos;
}
#endif

char *CSVReadLine(FILE *f)
{
    static char *buf = NULL;
    static size_t bufSize = 0;

    ssize_t nread = getline(&buf, &bufSize, f);

    if (nread == -1) {
        free(buf);
        bufSize = 0;
        return NULL;
    }

    if (buf[nread - 1] == '\n')
        nread--;

    char *newString = MemAlloc(nread + 1);
    memcpy(newString, buf, nread);
    newString[nread] = '\0';

    return newString;
}

char *CSVTokenize(char *line)
{
    char *tok = strtok(line, ",");
    if (!tok)
        return NULL;

    tok += strspn(tok, " \n\r\t");

    return tok;
}

bool CSVGetFieldNames(char *line, size_t numColumns, int *out, const char **names, size_t *result)
{
    if (!numColumns || !result)
        return false;

    *result = 0;

    for (size_t i = 0; i < numColumns; i++)
        out[i] = -1;

    char *tok = CSVTokenize(line);
    if (!tok)
        return false;

    while (tok) {
        for (size_t i = 0; i < numColumns; i++) {
            if (!strcmp(tok, names[i])) {
                out[i] = *result;
                break;
            }
        }

        (*result)++;
        if (*result == numColumns)
            break;

        tok = CSVTokenize(NULL);
    }

    return true;
}

bool CSVGetColumns(char *line, char **out, size_t numColumns, size_t *result)
{
    if (!numColumns || !result)
        return false;

    *result = 0;

    char *tok = CSVTokenize(line);
    while (tok) {
        out[*result] = tok;

        (*result)++;
        if (*result == numColumns)
            break;

        tok = CSVTokenize(NULL);
    }

    if (*result != numColumns)
        return false;

    return true;
}
