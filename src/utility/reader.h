/*
 * MIT License
 *
 * Copyright (c) 2021 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef READER_H
#define READER_H

#include <stdint.h>
#include <stdio.h>

#define DEF_READ_FUNCTION(funcName, type) type funcName(FILE *f)

DEF_READ_FUNCTION(ReadUint8, uint8_t);
DEF_READ_FUNCTION(ReadUint16, uint16_t);
DEF_READ_FUNCTION(ReadUint32, uint32_t);
DEF_READ_FUNCTION(ReadInt16, int16_t);
DEF_READ_FUNCTION(ReadInt32, int32_t);

#undef DEF_READ_FUNCTION

void ReadIntoBuffer(uint8_t *buffer, size_t length, FILE *f);

#endif
