/*
 * MIT License
 *
 * Copyright (c) 2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "path.h"

#include "string.h"
#include "memory.h"
#include "print.h"
#include "error.h"
#include "misc.h"

#include "../formats.h"
#include "../convert.h"

#include <stddef.h>
#include <stdio.h>
#include <string.h>

char *GetFileNamePath(const char *filename)
{
    if (!filename)
        return NULL;

    char *pathSep = strrchr(filename, '/');
    if (!pathSep)
        pathSep = strrchr(filename, '\\');
    if (!pathSep)
        return NULL;

    size_t len = pathSep - filename;
    char *path = MemAlloc(len + 1);
    memcpy(path, filename, len);
    path[len] = '\0';

    return path;
}

char *StripFileNamePath(const char *filename)
{
    if (!filename)
        return NULL;

    char *pathSep = strrchr(filename, '/');
    if (!pathSep)
        pathSep = strrchr(filename, '\\');
    if (!pathSep)
        return StringCopy(filename);

    if (pathSep[0] == '\0')
        return NULL;

    return StringCopy(pathSep + 1);
}

char *ConcatPaths(const char *pathA, const char *pathB)
{
    if (!pathA || !pathB)
        ThrowAbort("No paths to concatenate\n");

    size_t pathLenA = strlen(pathA);
    size_t pathLenB = strlen(pathB) + 1;

    size_t pathSize = pathLenA + pathLenB;
    bool hasPathSep = false;

    if (pathA[pathLenA - 1] == '/' || pathA[pathLenA - 1] == '\\')
        hasPathSep = true;
    else
        pathSize++;

    char *newPath = MemAlloc(pathSize);
    char *out = newPath;

    memcpy(newPath, pathA, pathLenA);

    newPath += pathLenA;

    if (!hasPathSep) {
        newPath[0] = '/';
        newPath++;
    }

    memcpy(newPath, pathB, pathLenB);
    return out;
}

const char *FindDotInFileName(const char *filename)
{
    size_t len = strlen(filename);

    if (!filename || !len)
        return NULL;

    const char *chr = filename + len - 1;

    while (chr > filename) {
        if (*chr == '/' || *chr == '\\')
            return NULL;
        else if (*chr == '.')
            return chr;

        chr--;
    }

    return NULL;
}

char *ConcatPathsOrDefault(const char *a, const char *b, bool strip)
{
    if (!b)
        abort();

    if (strip) {
        char *pathSep = strrchr(b, '/');
        if (!pathSep)
            pathSep = strrchr(b, '\\');
        if (pathSep)
            b = pathSep + 1;
    }

    if (a)
        return ConcatPaths(a, b);
    else
        return StringCopy(b);
}

char *MakeOutputSpriteFileName(const char *filename, const char *inputFn, int outFmt)
{
    const char *source;
    size_t length;

#ifdef TESTS_ENABLED
    static uint8_t testModeSuffixLen = 0;
#endif

    enum SpriteFormat extFmt = FORMAT_NONE;

    if (filename == NULL) {
        source = inputFn;
        if (!source)
            abort();

        length = strlen(source);

        // Find last '.'
        const char *dot = FindDotInFileName(source);
        if (dot)
            length = dot - source;

#ifdef TESTS_ENABLED
        // Remove the "_TESTxxx"
        if (length > testModeSuffixLen)
            length -= testModeSuffixLen;
#endif

        extFmt = outFmt;
    } else {
        source = filename;
        length = strlen(filename);
    }

    size_t allocLen = length;

    const char *jsonExt = ".json";
    const char *rsdkExt = ".bin";

    // Which file extension to suffix
    const char *extensionToUse = NULL;

#ifdef TESTS_ENABLED
    // "_TEST" (5)
    // 3 digits
    // ".json" (5)
    // NUL (1)
    char suffix[14];
#else
    const char *suffix = NULL;
#endif

    bool useSuffix = true;
    size_t suffixLen = 0;

    if (extFmt == FORMAT_JSON)
        extensionToUse = jsonExt;
    else if (extFmt == FORMAT_RSDK)
        extensionToUse = rsdkExt;
    else
        useSuffix = false;

#ifdef TESTS_ENABLED
    if (testMode) {
        snprintf(suffix, sizeof suffix, "_TEST%d", testModeIteration);
        testModeSuffixLen = strlen(suffix);

        if (useSuffix)
            memcpy(&suffix[testModeSuffixLen], extensionToUse, strlen(extensionToUse) + 1);
    } else
        snprintf(suffix, sizeof suffix, "%s", extensionToUse);
#else
    suffix = extensionToUse;
#endif

    if (useSuffix) {
        suffixLen = strlen(suffix);
        allocLen += suffixLen;
    }

    char *out = MemAlloc(allocLen + 1);
    memcpy(out, source, length);
    out[length] = '\0';

    if (useSuffix)
        memcpy(&out[length], suffix, suffixLen + 1);

    return out;
}

bool CheckFileNameExtension(const char *filename, const char *extension)
{
#ifdef DEBUG
    if (!filename || !extension)
        abort();
#endif

    // Find last '.'
    const char *dot = FindDotInFileName(filename);
    if (!dot)
        return false;

    return !strcmp(dot + 1, extension);
}

char *ChangeFileNameExtension(const char *filename, const char *extension)
{
#ifdef DEBUG
    if (!filename || !extension)
        abort();
#endif

    size_t fnLength = strlen(filename);
    size_t extLen = strlen(extension);

    // Find last '.'
    const char *dot = FindDotInFileName(filename);
    if (dot)
        fnLength = dot - filename;

    char *out = MemAlloc(fnLength + extLen + 1);
    memcpy(out, filename, fnLength);
    memcpy(&out[fnLength], extension, extLen + 1);

    return out;
}

static const char *formatExtensions[] = {
    [FORMAT_NONE] = "",
    [FORMAT_RSDK] = ".bin",
    [FORMAT_JSON] = ".json"
};

unsigned GuessFormatFromExtension(const char *filename)
{
    if (!filename)
        return FORMAT_NONE;

    const char *dot = FindDotInFileName(filename);
    if (!dot)
        return FORMAT_NONE;

    int format = EnumDictSearch(formatExtensions, FORMAT_NONE + 1, NUM_FORMATS, dot);
    if (format != -1)
        return format;

    return FORMAT_NONE;
}

unsigned GuessOutputFormat(int inputFormat, int fallbackFormat, const char *filename)
{
    if (filename) {
        unsigned format = GuessFormatFromExtension(filename);
        if (format != FORMAT_NONE)
            return format;
    }

    if (inputFormat == FORMAT_RSDK)
        return FORMAT_JSON;
    else if (inputFormat == FORMAT_JSON)
        return FORMAT_RSDK;
    else
        return fallbackFormat;
}
