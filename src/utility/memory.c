/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "memory.h"
#include "print.h"
#include "error.h"

#include <stdlib.h>

void *MemAlloc(size_t size)
{
#ifdef DEBUG
    if (!size)
        ThrowAbort("Can't allocate with size zero\n");
#endif

    void *memory = malloc(size);
    if (!memory)
        ThrowAbort("Out of memory\n");

    return memory;
}

void *MemCalloc(size_t size)
{
#ifdef DEBUG
    if (!size)
        ThrowAbort("Can't allocate with size zero\n");
#endif

    void *memory = calloc(1, size);
    if (!memory)
        ThrowAbort("Out of memory\n");

    return memory;
}

void *MemRealloc(void *memory, size_t size)
{
#ifdef DEBUG
    if (!size)
        ThrowAbort("Can't reallocate with size zero\n");
#endif

    memory = realloc(memory, size);
    if (!memory)
        ThrowAbort("Out of memory\n");

    return memory;
}
