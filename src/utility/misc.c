/*
 * MIT License
 *
 * Copyright (c) 2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "misc.h"

#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>

int min(int a, int b)
{
    if (a < b)
        return a;
    else
        return b;
}

int max(int a, int b)
{
    if (a > b)
        return a;
    else
        return b;
}

int EnumDictSearch(const char **dict, size_t start, size_t size, const char *check)
{
    for (size_t i = start; i < size; i++) {
        if (!strcmp(check, dict[i]))
            return i;
    }

    return -1;
}

int StringToNumber(const char *string, int *number)
{
    char *end;
    long num = strtol(string, &end, 10);

    if (*end != '\0')
        return STRING_TO_NUMBER_INVALID;
    else if (num > INT_MAX || (errno == ERANGE && num == LONG_MAX))
        return STRING_TO_NUMBER_RANGE_MIN;
    else if (num < INT_MIN || (errno == ERANGE && num == LONG_MIN))
        return STRING_TO_NUMBER_RANGE_MAX;

    *number = num;

    return STRING_TO_NUMBER_OK;
}
