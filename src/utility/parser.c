/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "parser.h"
#include "memory.h"
#include "string.h"
#include "print.h"
#include "error.h"
#include "misc.h"

#include "../formats/json.h"

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

static size_t currentLine;
static size_t currentColumn;

static void ResetScanner(void)
{
    currentLine = 1;
    currentColumn = 0;
}

static void ThrowParseError(const char *format, ...)
{
    char buffer[VARIADIC_BUF_SIZE];
    VARIADIC_FORMAT(buffer, format);
    ThrowError("%s at line %d, column %d", buffer, currentLine, currentColumn);
}

static void WhileReadingError(const char *error, const char *reading)
{
    ThrowParseError("%s while reading JSON %s", error, reading);
}

#define WHILE_READING_ERROR(str, a, b, c) { \
    char buffer[VARIADIC_BUF_SIZE]; \
    snprintf(buffer, sizeof buffer, str, a, b); \
    WhileReadingError(buffer, reading); \
}

static void ErrorExpected(const char *expected, char got, const char *reading)
{
    WHILE_READING_ERROR("Expected %s but got '%c'", expected, got, reading);
}

static void ErrorExpectedChar(char expected, char got, const char *reading)
{
    WHILE_READING_ERROR("Expected '%c' but got '%c'", expected, got, reading);
}

#undef WHILE_READING_ERROR

static void ErrorEOF(const char *reading)
{
    WhileReadingError("Unexpected EOF", reading);
}

static void ErrorUnexpected(const char *unexpected, const char *reading)
{
    char buffer[VARIADIC_BUF_SIZE];
    snprintf(buffer, sizeof buffer, "Unexpected %s", unexpected);
    WhileReadingError(buffer, reading);
}

static void ErrorUnexpectedChar(char c, const char *reading)
{
    char buffer[VARIADIC_BUF_SIZE];
    snprintf(buffer, sizeof buffer, "Unexpected character '%c'", c);
    WhileReadingError(buffer, reading);
}

static int ScanText(FILE *f)
{
    int chr = fgetc(f);
    if (chr == EOF)
        return -1;

    if (ferror(f))
        ThrowParseError("Error while reading JSON file");
    else if (feof(f))
        return -1;

    currentColumn++;

    if (chr == '\n') {
        currentColumn = 1;
        currentLine++;
    }

    return chr;
}

static int PeekText(FILE *f)
{
    size_t pos = ftell(f);

    int chr = fgetc(f);
    if (chr == EOF)
        return -1;

    if (ferror(f))
        ThrowParseError("Error while reading JSON file");
    else if (feof(f))
        return -1;

    fseek(f, pos, SEEK_SET);

    return chr;
}

static bool SeekScanner(FILE *f, size_t position)
{
    ResetScanner();

    fseek(f, 0, SEEK_SET);

    for (size_t i = 0; i < position; i++) {
        if (ScanText(f) < 0)
            return false;
    }

    return true;
}

static bool IsWhitespace(char c)
{
    return isspace(c);
}

static struct JSONValue JSONParseValue(struct JSONValue *key, char chr, FILE *f, const char *from);
static void JSONParseObject(struct JSONObject *obj, FILE *f);

#define SCAN_FILE(f) \
    chr = ScanText(f); \
    if (chr < 0) \
        ErrorEOF(from)

#define PEEK_FILE(f) \
    chr = PeekText(f); \
    if (chr < 0) \
        ErrorEOF(from)

#define SEEK_FILE(f, position) \
    if (!SeekScanner(f, position)) \
        ErrorEOF(from)

#define GENERIC_ERROR(str, arg) { \
    char buffer[VARIADIC_BUF_SIZE]; \
    snprintf(buffer, sizeof buffer, str, arg); \
    WhileReadingError(buffer, from); \
}

#define SKIP_WHITESPACE(f) \
    while (IsWhitespace((char)scanned)) \
        scanned = ScanText(f); \
    if (scanned < 0) \
        ErrorEOF(currParse)

static size_t EscapeUnicode(char *string, FILE *f, const char *from)
{
    unsigned i, seq = 0;

    int chr;
    SCAN_FILE(f);

    for (i = 0; i < 7;) {
        seq <<= 4;

        if (chr >= '0' && chr <= '9')
            seq |= chr - '0';
        else
            seq |= (tolower(chr) - 'a') + 10;

        i++;
        PEEK_FILE(f);

        if (!isxdigit(chr))
            break;

        SCAN_FILE(f);
    }

    if (i != 4) {
        GENERIC_ERROR("Invalid Unicode escape sequence (must have 4 digits, but had %d instead)", i);
    } else {
        if (seq <= 0x007F) {
            if (string != NULL)
                string[0] = seq & 0x7F; // 0xxxxxxx

            return 1;
        } else if (seq <= 0x000007FF) {
            if (string != NULL) {
                string[0] = 0xC0; // 110xxxxx
                string[1] = 0x80; // 10xxxxxx

                string[0] |= (seq >> 6) & 0x1F;
                string[1] |= seq & 0x3F;
            }

            return 2;
        } else if (seq <= 0x0000FFFF) {
            if (string != NULL) {
                string[0] = 0xE0; // 1110xxxx
                string[1] = string[2] = 0x80; // 10xxxxxx

                string[0] |= (seq >> 12) & 0x0F;
                string[1] |= (seq >> 6) & 0x3F;
                string[2] |= seq & 0x3F;
            }

            return 3;
        } else if (seq <= 0x001FFFFF) {
            if (string != NULL) {
                string[0] = 0xF0; // 11110xxx
                string[1] = string[2] = string[3] = 0x80; // 10xxxxxx

                string[0] |= (seq >> 18) & 0x07;
                string[1] |= (seq >> 12) & 0x3F;
                string[2] |= (seq >> 6) & 0x3F;
                string[3] |= seq & 0x3F;
            }

            return 4;
        } else
            WhileReadingError("Invalid Unicode escape sequence", from);
    }

    // Unreachable
    return 0;
}

// Returns '\0' if escaped
static char EscapeChar(char *string, size_t *i, char chr, FILE *f, const char *from)
{
    if (chr != '\\') {
        if (string)
            string[(*i)++] = chr;
        return chr;
    }

    int scanned = ScanText(f);
    if (scanned < 0)
        ErrorEOF(from);

#define ESCAPE(esc) \
    if (string) \
        string[*i] = esc; \
    (*i)++; \
    break

    switch (scanned) {
        case 'u':
            if (string)
                (*i) += EscapeUnicode(&string[*i], f, from);
            else
                (*i) += EscapeUnicode(NULL, f, from);
            break;
        case 'b': ESCAPE('\b');
        case 'f': ESCAPE('\f');
        case 'n': ESCAPE('\n');
        case 'r': ESCAPE('\r');
        case 't': ESCAPE('\t');
        case '/': ESCAPE('/');;
        case '\\': ESCAPE('\\');
        case '\"': ESCAPE('\"');
        default:
            GENERIC_ERROR("Invalid escape sequence \\%c", scanned);
    }

#undef ESCAPE

    return '\0';
}

static struct JSONValue JSONParseString(FILE *f, const char *from)
{
    struct JSONValue val;

    val.type = JSON_STRING;

    size_t stringSize = 0;
    size_t start = ftell(f);

    while (true) {
        // The last line and column are saved and restored for
        // showing errors about strings that span multiple lines.
        size_t lastLine = currentLine;
        size_t lastColumn = currentColumn;

        int scanned = ScanText(f);
        if (scanned < 0)
            ErrorEOF(from);

        char current = EscapeChar(NULL, &stringSize, (char)scanned, f, from);

        if (current != '\0') {
            if (current == '"')
                break;
            else if (current == '\n' || current == '\r') {
                currentLine = lastLine;
                currentColumn = lastColumn;

                ErrorUnexpected("newline in the middle of a string", from);
            }

            stringSize++;
        }
    }

    // Allocate string for the value
    val.u.string = MemAlloc(stringSize + 1);

    SEEK_FILE(f, start);
    size_t i = 0;

    while (true) {
        int scanned = ScanText(f);
        if (scanned < 0)
            ErrorEOF(from);

        EscapeChar(val.u.string, &i, (char)scanned, f, from);

        if (i == stringSize)
            break;
    }

    val.u.string[i] = '\0';

    if (ScanText(f) < 0)
        ErrorEOF(from);

    return val;
}

static struct JSONValue JSONParseNumber(FILE *f, const char *from)
{
    struct JSONValue val;
    int chr;

    val.type = JSON_NUMBER;

    size_t start = ftell(f) - 1;

    while (true) {
        SCAN_FILE(f);

        if (chr >= '0' && chr <= '9')
            continue;
        else if (IsWhitespace(chr) || chr == ',' || chr == ']' || chr == '}')
            break;
        else
            GENERIC_ERROR("Unexpected character '%c' in the middle of a number", chr);
    }

    // Allocate string with number
    size_t stringSize = (ftell(f) - 1) - start;
    char *numStr = MemAlloc(stringSize + 1);

    SEEK_FILE(f, start);
    size_t i = 0;

    while (stringSize--) {
        SCAN_FILE(f);
        numStr[i++] = (char)chr;
    }

    numStr[i] = '\0';

    // Parse the number
    int num;

    if (StringToNumber(numStr, &num) != STRING_TO_NUMBER_OK) {
        GENERIC_ERROR("Invalid number \"%s\"", numStr);
    }

    free(numStr);

    val.u.number = num;

    return val;
}

#undef GENERIC_ERROR

static struct JSONValue JSONParseKeyword(FILE *f, const char *from)
{
    struct JSONValue val;
    int chr;

    size_t start = ftell(f);

    while (true) {
        SCAN_FILE(f);

        if (chr == ',' || chr == ']' || chr == '}')
            break;
    }

    // Allocate string with the keyword
    size_t keywordSize = (ftell(f) - 1) - start;
    char *keyword = MemAlloc(keywordSize + 1);

    SEEK_FILE(f, start);
    size_t i = 0;

    while (keywordSize--) {
        SCAN_FILE(f);
        keyword[i++] = (char)chr;
    }

    keyword[i] = '\0';

    if (!strcmp(keyword, "true"))
        val.type = JSON_TRUE;
    else if (!strcmp(keyword, "false"))
        val.type = JSON_FALSE;
    else if (!strcmp(keyword, "null"))
        val.type = JSON_NULL;
    else {
        char buffer[VARIADIC_BUF_SIZE];
        snprintf(buffer, sizeof buffer, "Unexpected keyword \"%s\"", keyword);
        WhileReadingError(buffer, from);
    }

    free(keyword);

    return val;
}

#undef SCAN_FILE
#undef SEEK_FILE

static struct JSONArray *JSONArrayAlloc(void)
{
    struct JSONArray *array = MemAlloc(sizeof(*array));

    array->capacity = 8;
    array->numValues = 0;

    array->values = NULL;

    return array;
}

static void JSONArrayRealloc(struct JSONArray *array)
{
    array->values = MemRealloc(array->values, sizeof(struct JSONValue) * array->capacity);
}

static void JSONArrayPush(struct JSONArray *array, struct JSONValue *value)
{
    int index = array->numValues;

    array->numValues++;

    if (array->values == NULL)
        JSONArrayRealloc(array);
    else if (array->numValues == array->capacity) {
        array->capacity *= 2;

        JSONArrayRealloc(array);
    }

    memcpy(&array->values[index], value, sizeof(struct JSONValue));
}

static void JSONParseArray(struct JSONArray *array, FILE *f)
{
    const char *currParse = "array";

    int scanned = ScanText(f);
    if (scanned < 0)
        ErrorEOF(currParse);

    SKIP_WHITESPACE(f);

    struct JSONValue key;
    key.type = JSON_NUMBER;

    uint8_t status = 0;

    while (true) {
        char chr = (char)scanned;

        switch (chr) {
            case ',':
                if (status != 1)
                    ErrorUnexpectedChar(chr, currParse);
                status = 2;
                break;
            case ']':
                if (status == 2)
                    ErrorExpected("value", ']', currParse);
                return;
            case -1:
                ErrorEOF(currParse);
                return;
            default:
                if (status == 1)
                    ErrorUnexpectedChar(chr, currParse);

                key.u.number = array->numValues;

                // Parse value
                struct JSONValue value = JSONParseValue(&key, chr, f, currParse);

                JSONArrayPush(array, &value);

                status = 1;
                break;
        }

        scanned = ScanText(f);

        while (IsWhitespace((char)scanned))
            scanned = ScanText(f);
    }
}

static struct JSONObject *JSONObjectAlloc(void)
{
    struct JSONObject *obj = MemAlloc(sizeof(*obj));

    obj->capacity = 8;
    obj->numValues = 0;

    obj->keys = NULL;
    obj->values = NULL;

    return obj;
}

static void JSONObjectRealloc(struct JSONObject *obj)
{
    obj->keys = MemRealloc(obj->keys, sizeof(struct JSONValue) * obj->capacity);
    obj->values = MemRealloc(obj->values, sizeof(struct JSONValue) * obj->capacity);
}

static void JSONObjectPush(struct JSONObject *obj, struct JSONValue *key, struct JSONValue *value)
{
    int index = obj->numValues;

    obj->numValues++;

    if (obj->keys == NULL || obj->values == NULL)
        JSONObjectRealloc(obj);
    else if (obj->numValues == obj->capacity) {
        obj->capacity *= 2;

        JSONObjectRealloc(obj);
    }

    memcpy(&obj->keys[index], key, sizeof(struct JSONValue));
    memcpy(&obj->values[index], value, sizeof(struct JSONValue));
}

#ifdef JSON_PARSER_DEBUG
size_t dbgCurDepth = 0;

static void DebugKeyValuePair(struct JSONValue *key, struct JSONValue *value)
{
    size_t indent = (dbgCurDepth * 4);

    if (key) {
        for (size_t i = 0; i < indent; i++)
            fputc(' ', stdout);

        printf("| Key: ");

        if (key->type == JSON_STRING)
            printf("\"%s\"", key->u.string);
        else if (key->type == JSON_NUMBER)
            printf("%d", key->u.number);
        else
            fputc('?', stdout);
    }

    fputc('\n', stdout);

    for (size_t i = 0; i < indent; i++)
        fputc(' ', stdout);

    printf("| Value: ");

    switch (value->type) {
        case JSON_STRING:
            printf("\"%s\"", value->u.string);
            break;
        case JSON_NUMBER:
            printf("%d", value->u.number);
            break;
        case JSON_OBJECT: printf("<object>"); break;
        case JSON_ARRAY: printf("<array>"); break;
        case JSON_TRUE: printf("true"); break;
        case JSON_FALSE: printf("false"); break;
        case JSON_NULL: printf("null"); break;
        default: printf("?");
    }

    printf("\n");
}
#endif

static struct JSONValue ParseObjectOrArray(struct JSONValue *key, char chr, FILE *f, const char *from)
{
    struct JSONValue val;

#ifndef JSON_PARSER_DEBUG
    (void)key;
    (void)from;
#endif

    if (chr == '{') {
        struct JSONObject *obj = JSONObjectAlloc();

        val.u.object = obj;
        val.type = JSON_OBJECT;

#ifdef JSON_PARSER_DEBUG
        DebugKeyValuePair(key, &val);
        dbgCurDepth++;
#endif

        JSONParseObject(obj, f);
    } else {
        struct JSONArray *array = JSONArrayAlloc();

        val.u.array = array;
        val.type = JSON_ARRAY;

#ifdef JSON_PARSER_DEBUG
        DebugKeyValuePair(key, &val);
        dbgCurDepth++;
#endif

        JSONParseArray(array, f);
    }

#ifdef JSON_PARSER_DEBUG
    dbgCurDepth--;
#endif

    return val;
}

static struct JSONValue JSONParseValue(struct JSONValue *key, char chr, FILE *f, const char *from)
{
    struct JSONValue val;

    switch (chr) {
        case '"':
            val = JSONParseString(f, from);
            break;
        case '{':
        case '[':
            val = ParseObjectOrArray(key, chr, f, from);
            return val;
        case -1:
            ErrorEOF(from);
            break;
        default:
            if ((chr >= '0' && chr <= '9') || chr == '-') {
                val = JSONParseNumber(f, from);
                break;
            }

            val = JSONParseKeyword(f, from);
    }

#ifdef JSON_PARSER_DEBUG
    DebugKeyValuePair(key, &val);
#endif

    return val;
}

static void JSONParseObject(struct JSONObject *obj, FILE *f)
{
    const char *currParse = "object";

    int scanned = ScanText(f);
    if (scanned < 0)
        ErrorEOF(currParse);

    uint8_t status = 0;

    while (true) {
        char chr = (char)scanned;

        switch (chr) {
            case '"': {
                struct JSONValue key, value;

                if (status == 1)
                    ErrorUnexpectedChar(chr, currParse);

                // Parse key
                key = JSONParseString(f, currParse);

                // Skip whitespace
                SKIP_WHITESPACE(f);

                // Scan ':'
                scanned = ScanText(f);
                if (scanned < 0)
                    ErrorEOF(currParse);
                else if (scanned != ':')
                    ErrorExpectedChar(':', chr, currParse);

                scanned = ScanText(f);
                if (scanned < 0)
                    ErrorEOF(currParse);

                // Skip whitespace
                SKIP_WHITESPACE(f);

                // Parse value
                value = JSONParseValue(&key, (char)scanned, f, currParse);

                JSONObjectPush(obj, &key, &value);

                status = 1;
                break;
            }
            case ',':
                if (status != 1)
                    ErrorUnexpectedChar(chr, currParse);
                status = 2;
                break;
            case '}':
                if (status == 2)
                    ErrorExpected("key", chr, currParse);
                return;
            case -1:
                ErrorEOF(currParse);
                return;
            default:
                if (!IsWhitespace(chr))
                    ErrorUnexpectedChar(chr, currParse);
        }

        scanned = ScanText(f);

        while (IsWhitespace((char)scanned))
            scanned = ScanText(f);
    }
}

#undef SKIP_WHITESPACE

static void SetRootKey(struct JSONValue *rootKey)
{
    rootKey->type = JSON_STRING;
    rootKey->u.string = StringCopy("<root>");
}

struct JSONValue JSONParseText(FILE *f)
{
    struct JSONValue rootKey, rootValue;

    const char *currParse = "file";

    ResetScanner();

    int scanned = ScanText(f);
    if (scanned < 0)
        ErrorEOF(currParse);

    SetRootKey(&rootKey);
    rootValue.type = JSON_UNKNOWN;

    while (true) {
        char chr = (char)scanned;

        if (rootValue.type != JSON_UNKNOWN) {
            if (IsWhitespace(chr)) {
                scanned = ScanText(f);
                continue;
            }

            if (scanned < 0)
                break;

            ErrorUnexpected("non-whitespace character near end", currParse);
        }

        switch (chr) {
            case '{':
            case '[':
                rootValue = ParseObjectOrArray(&rootKey, chr, f, currParse);
                break;
            case -1:
                ErrorEOF(currParse);
                break;
            default:
                if (!IsWhitespace(chr))
                    ErrorUnexpectedChar(chr, currParse);
        }

        scanned = ScanText(f);

        while (IsWhitespace((char)scanned))
            scanned = ScanText(f);
    }

    free(rootKey.u.string);

    return rootValue;
}
