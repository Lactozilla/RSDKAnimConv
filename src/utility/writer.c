/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "writer.h"
#include "file.h"
#include "print.h"
#include "error.h"

#define DEF_WRITE_FUNCTION(funcName, type) \
void funcName(type in, FILE *f) \
{ \
    if (fwrite(&in, sizeof in, 1, f) != 1) \
        ThrowError("Couldn't write file %s", GetIOFileName(f)); \
}

DEF_WRITE_FUNCTION(WriteUint8, uint8_t);
DEF_WRITE_FUNCTION(WriteUint16, uint16_t);
DEF_WRITE_FUNCTION(WriteUint32, uint32_t);
DEF_WRITE_FUNCTION(WriteInt16, int16_t);
DEF_WRITE_FUNCTION(WriteInt32, int32_t);

#undef DEF_WRITE_FUNCTION

void WriteFromBuffer(uint8_t *buffer, size_t length, FILE *f)
{
    if (fwrite(buffer, sizeof (*buffer), length, f) != length)
        ThrowError("Couldn't write file %s", GetIOFileName(f));
}
