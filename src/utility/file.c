/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "file.h"
#include "memory.h"
#include "string.h"
#include "print.h"
#include "error.h"
#include "misc.h"

#include "../sprite.h"

#ifdef TESTS_ENABLED
#include "../convert.h"
#endif

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

static char *inputFileName = NULL;
static char *outputFileName = NULL;

static FILE *inputFile = NULL;
static FILE *outputFile = NULL;

FILE *OpenInputFile(const char *filename)
{
    if (filename == NULL)
        ThrowAbort("No input file to open");

    if (inputFileName != NULL)
        ThrowAbort("Input file already open");

#ifdef FILE_DEBUG
    printf("Opening input file %s...\n", filename);
#endif

    if (inputFile != NULL)
        ThrowAbort("Input file already open");

    inputFile = fopen(filename, "rb");
    if (inputFile) {
        inputFileName = StringCopy(filename);
        return inputFile;
    }

    return NULL;
}

FILE *OpenOutputFile(const char *filename)
{
    if (filename == NULL)
        ThrowAbort("No output file to open");

    if (outputFileName != NULL)
        ThrowAbort("Output file already open");

#ifdef FILE_DEBUG
    printf("Opening output file %s...\n", filename);
#endif

    if (outputFile != NULL)
        ThrowAbort("Output file already open");

    outputFile = fopen(filename, "wb");
    if (outputFile) {
        outputFileName = StringCopy(filename);
        return outputFile;
    }

    return NULL;
}

void CloseInputFile(void)
{
    if (inputFile == NULL || inputFileName == NULL)
        ThrowAbort("No input file to close");

#ifdef FILE_DEBUG
    printf("Closing input file %s...\n", inputFileName);
#endif

    free(inputFileName);
    inputFileName = NULL;

    fclose(inputFile);
    inputFile = NULL;
}

void CloseOutputFile(void)
{
    if (outputFile == NULL || outputFileName == NULL)
        ThrowAbort("No output file to close");

#ifdef FILE_DEBUG
    printf("Closing output file %s...\n", outputFileName);
#endif

    free(outputFileName);
    outputFileName = NULL;

    fclose(outputFile);
    outputFile = NULL;
}

const char *GetIOFileName(FILE *f)
{
    if (f == inputFile && inputFileName != NULL)
        return inputFileName;
    else if (f == outputFile && outputFileName != NULL)
        return outputFileName;
    else
        return "(?)";
}
