/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "convert.h"

#include "main.h"
#include "args.h"
#include "input.h"
#include "sprite.h"

#include "utility/file.h"
#include "utility/path.h"
#include "utility/print.h"
#include "utility/error.h"

#include "formats/rsdk.h"
#include "formats/json.h"

#include <stdlib.h>
#include <string.h>

struct ProgramArgument cmdArgs_convert[] = {
    COMMON_CMD_ARGS,
    COMMON_EXPORT_CMD_ARGS,

    {"from", ARG_OPTION, .option = {inputFormatDict, &inputFormat}},
    {"to", ARG_OPTION, .option = {outputFormatDict, &outputFormat}},

#ifdef TESTS_ENABLED
    {"test", ARG_FLAG, .flag = &testMode},
#endif

    END_CMD_ARGS
};

#ifdef TESTS_ENABLED
bool testMode = false;
uint8_t testModeIteration = 0;

static const char *formatNames[] = {
    [FORMAT_NONE] = "none",
    [FORMAT_RSDK] = "rsdk",
    [FORMAT_JSON] = "json"
};
#endif

static char *DoConversion(const char *inputFn, const char *outputFn)
{
    FILE *file = OpenInputSprite(inputFn);
    if (file == NULL)
        ThrowError("Couldn't open input sprite");

    // Fallback to JSON
    if (inputFormat == FORMAT_NONE)
        inputFormat = FORMAT_JSON;

    struct RSDKSprite *inputSprite = ReadSpriteFromFile(file);
    if (inputSprite == NULL)
        ThrowError("Couldn't read input sprite");

    CloseInputFile();

    // Autodetect output format
    if (outputFormat == FORMAT_NONE)
        outputFormat = GuessOutputFormat(inputFormat, FORMAT_RSDK, outputFn);

    // Make the output filename
    char *outFileName = MakeOutputSpriteFileName(outputFn, inputFn, outputFormat);

#ifdef TESTS_ENABLED
    if (testMode) {
        printf("Test %d: converting from %s to %s... (%s)\n",
            testModeIteration,
            formatNames[inputFormat], formatNames[outputFormat],
            outFileName);
    }
#endif

    if (!SaveOutputSprite(outFileName, inputSprite))
        ThrowError("Couldn't write output sprite");

    SpriteDelete(inputSprite);

    return outFileName;
}

#ifdef TESTS_ENABLED
static void ConvertSpriteFilename(char *inputFn, char *outputFn)
#else
static void ConvertSpriteFilename(const char *inputFn, const char *outputFn)
#endif
{
#ifdef TESTS_ENABLED
    char *filename = NULL;

    if (!testMode)
        filename = DoConversion(inputFn, outputFn);
    else {
        testModeIteration = 1;

        // Ignore output parameters
        outputFn = NULL;
        outputFormat = FORMAT_NONE;

        for (size_t i = 0; i < 5; i++) {
            filename = DoConversion(inputFn, outputFn);

            if (i)
                free(inputFn);

            inputFn = filename;
            outputFn = NULL;

            inputFormat = outputFormat;
            outputFormat = FORMAT_NONE;

            testModeIteration++;
        }
    }
#else
    char *filename = DoConversion(inputFn, outputFn);
#endif

    free(filename);
}

void ConvertSprites(struct List *inputList, const char *outputFn)
{
    if (!inputList)
        ThrowError("No input files");

    if (ListSize(inputList) > 1)
        outputFn = NULL;

    for (size_t i = 0; i < ListSize(inputList); i++)
        ConvertSpriteFilename(ListGet(inputList, i), (char*)outputFn);
}
