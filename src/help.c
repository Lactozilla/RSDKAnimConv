/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "help.h"
#include "main.h"
#include "args.h"

#include "sprite.h"
#include "sheet.h"

#include "formats/json.h"

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

struct ArgumentHelp {
    const char *argument;
    const char *explanation;

    struct ArgumentOptions *options;
    const char *defaultOption;

    bool optional;
};

struct ArgumentOptions {
    const char *option;
    const char *explanation;

    int value;
    bool isDefault;
};

static struct ArgumentOptions fromToArgHelp[] = {
    {"rsdk", "RSDK v5 format", FORMAT_RSDK, false},
    {"json", "JSON format", FORMAT_JSON, false},

    {NULL, NULL, 0, false}
};

static struct ArgumentOptions jsonExportModeArgHelp[] = {
    {"hatch",          "Hatch Game Engine sprite format", JSON_EXPORT_HATCH, true},
    {"rsdkanimeditor", "RSDK Animation Editor format", JSON_EXPORT_RSDKANIMEDITOR, false},
    {"retroed2",       "RetroED2 format", JSON_EXPORT_RETROED2, false},

    {NULL, NULL, 0, 0}
};

#define COMMON_HELP_ARGS \
    {"input",  "A filename or path to the input sprite.", NULL, NULL, false}, \
    {"output", "A filename or path to the output sprite.", NULL, NULL, true}

#define COMMON_EXPORT_HELP_ARGS \
    {"jsonExportMode", "When the output is a JSON sprite, " \
                       "this sets the specification.", \
                       jsonExportModeArgHelp, NULL, true}, \
 \
    {"jsonIndentSize", "When the output is a JSON sprite, " \
                       "this sets the indentation size.", \
                 NULL, "The default size is 2.", true}, \
 \
    {"jsonEscapeSlashes", "When the output is a JSON sprite, " \
                          "this defines whether to write " \
                          "escaped forward slashes (/) or not.", \
                    NULL, "By default, this is disabled.", true}

#define END_HELP_ARGS {NULL, NULL, NULL, NULL, false}

static struct ArgumentHelp argumentHelp_convert[] = {
    COMMON_HELP_ARGS,

    {"from", "The sprite format to convert from. "
             "If omitted, this will be guessed from the "
             "input sprite.",
             fromToArgHelp, NULL, true},

    {"to", "The sprite format to convert to. "
           "If omitted, an appropriate format will be chosen.",
           fromToArgHelp, NULL, true},

    COMMON_EXPORT_HELP_ARGS,
    END_HELP_ARGS
};

static struct ArgumentOptions frameSortModeArgHelp[] = {
    {"none", "Don't sort", SHEET_SORT_NONE, false},
    {"width", "Sort by width", SHEET_SORT_WIDTH, false},
    {"height", "Sort by height", SHEET_SORT_HEIGHT, false},
    {"maxside", "Sort by max(width, height)", SHEET_SORT_MAXSIDE, true},
    {"area", "Sort by area", SHEET_SORT_AREA, false},

    {NULL, NULL, 0, false}
};

static struct ArgumentHelp argumentHelp_create[] = {
    {"input",  "A filename or path to the input animation. "
                "This may be an image, tabular animation data in the CSV format, or a sprite.", NULL, NULL, true},
    {"output", "A filename or path to the output sprite.", NULL, "This is only optional if --input was specified.", false},

    {"outputSheetPath", "The path for the output sprite's sheets.", NULL, NULL, true},
    {"animationName", "The default name for the sprite's animations.", NULL, NULL, true},

    {"cropFrames", "If enabled, frames will be cropped to remove any excess empty space.", NULL, NULL, true},
    {"mergeFrames", "If enabled, similar adjacent frames are merged together.", NULL, NULL, true},
    {"centerFrames", "If enabled, frames are centered.", NULL, NULL, true},
    {"frameSortMode", "Which sorting algorithm is used for packing the frames inside the output sheets.",
        frameSortModeArgHelp, NULL, true},

    {"maxSheetWidth", "The maximum width of a sheet. If no more frames can fit in a sheet, a new one will be created.", NULL, NULL, true},
    {"maxSheetHeight", "The maximum height of a sheet. If no more frames can fit in a sheet, a new one will be created.", NULL, NULL, true},

    {"removeBackground", "Removes the specified color from the input image, making the color transparent. "
                        "An HTML color or a \"r:<value> g:<value> b:<value> a:<value>\" "
                        "string are supported.", NULL, NULL, true},

    {"format", "The format the output sprite will be created with. "
                "If omitted, an appropriate format will be chosen.", NULL, NULL, true},

    COMMON_EXPORT_HELP_ARGS,
    END_HELP_ARGS
};

#undef COMMON_HELP_ARGS
#undef COMMON_EXPORT_HELP_ARGS
#undef END_HELP_ARGS

static struct ArgumentHelp *argumentHelp[] = {
    NULL,
    argumentHelp_convert,
    argumentHelp_create
};

struct CommandHelp {
    const char *command;
    const char *explanation;
};

static struct CommandHelp commandHelp[] = {
    {"convert", "Converts a sprite into another format."},
    {"create", "Creates a sprite using sheets."},

    {NULL, NULL}
};

static size_t maxTerminalWidth = 80;
static size_t argSpacesBefore = 4;
static size_t descSpacesBefore, optionsSpacesBefore;
static size_t maxDescWidth;

static void WriteSpaces(size_t num)
{
    for (size_t i = 0; i < num; i++)
        fputc(' ', stdout);
}

static size_t WriteWordWrapped(const char *text, size_t spacesBefore, size_t maxWidth)
{
    size_t descLen = strlen(text);
    size_t writtenOnLine = 0;

    for (size_t j = 0; j < descLen; j++) {
        char chr = text[j];
        bool doWordWrap = false;

        writtenOnLine++;

        // Check if the next word would go over the max width
        if (chr == ' ') {
            if (writtenOnLine >= maxWidth)
                doWordWrap = true;
            else {
                size_t n = writtenOnLine;

                for (size_t k = j + 1; k < descLen; k++) {
                    char c = text[k];
                    n++;

                    if (c == ' ' || c == '.' || c == ',' || c == '\0')
                        break;
                }

                if (n >= maxWidth)
                    doWordWrap = true;
            }
        }

        if (doWordWrap) {
            printf("\n");
            WriteSpaces(spacesBefore);
            writtenOnLine = 0;
        } else
            printf("%c", chr);
    }

    return writtenOnLine;
}

static bool WriteStringOrWrap(const char *text, size_t written, size_t spacesBefore, size_t maxWidth)
{
    bool wrapped = false;

    // Check if the text can be shown without wrapping
    if (written + strlen(text) >= maxWidth) {
        printf("\n");
        WriteSpaces(spacesBefore);
        wrapped = true;
    } else
        WriteSpaces(1);

    printf("%s", text);

    return wrapped;
}

const char *GetUsageText(void)
{
    return "<command> --input <filename> [--output <filename>] [<args>] [--help [<command>]]";
}

void ShowUsage(void)
{
    printf("usage: %s %s\n", GetProgramName(), GetUsageText());
}

static void ShowCommandHelp(void)
{
    descSpacesBefore = argSpacesBefore + 20;
    optionsSpacesBefore = descSpacesBefore + 4;

    maxDescWidth = (maxTerminalWidth - descSpacesBefore);

    printf("\nSupported commands:\n");

    for (size_t i = 0; commandHelp[i].command; i++) {
        struct CommandHelp *help = &commandHelp[i];

        WriteSpaces(argSpacesBefore);
        printf("%s", help->command);

        WriteSpaces(descSpacesBefore - argSpacesBefore - strlen(help->command));
        WriteWordWrapped(help->explanation, descSpacesBefore, maxDescWidth);

        printf("\n");
    }

    printf("\n");
}

void ShowHelp(int programCommand, struct ProgramArgument *progArgList)
{
    if (programCommand == COMMAND_NONE) {
        ShowUsage();
        ShowCommandHelp();
        return;
    }

    FindArgsForHelp(progArgList);

    descSpacesBefore = argSpacesBefore + 20;
    optionsSpacesBefore = descSpacesBefore + 4;

    maxDescWidth = (maxTerminalWidth - descSpacesBefore);

    struct ArgumentHelp *helpList = argumentHelp[programCommand];

    printf("Showing supported arguments for %s:\n\n", programCommandList[programCommand]);

    for (size_t i = 0, numShown = 0; helpList[i].argument; i++) {
        struct ArgumentHelp *help = &helpList[i];

        int helpArgOption = -1;
        if (HasHelpArgs() && !SearchInHelpArgs(help->argument, &helpArgOption))
            continue;

        if (numShown > 0)
            printf("\n\n");

        numShown++;

        WriteSpaces(argSpacesBefore);
        printf("%s%s", ARG_DASHES, help->argument);

        WriteSpaces(descSpacesBefore - argSpacesBefore - (sizeof(ARG_DASHES) - 1) - strlen(help->argument));

        // Write description
        size_t written = WriteWordWrapped(help->explanation, descSpacesBefore, maxDescWidth);

        if (help->optional) {
            const char *optionalText = "This argument is optional.";

            if (!WriteStringOrWrap(optionalText, written, descSpacesBefore, maxDescWidth))
                written += strlen(optionalText);
        }

        if (help->defaultOption)
            WriteStringOrWrap(help->defaultOption, written, descSpacesBefore, maxDescWidth);

        // Write options
        if (help->options) {
            printf("\n\n");
            WriteSpaces(descSpacesBefore);

            if (HasHelpArgs() && helpArgOption != -1)
                printf("Chosen option:\n");
            else
                printf("Possible options:\n");

            for (size_t j = 0;;) {
                struct ArgumentOptions *opt = &help->options[j];
                static char buf[512];

                if (HasHelpArgs() && helpArgOption != -1 && opt->value != helpArgOption) {
                    j++;
                    if (!help->options[j].option)
                        break;
                    else
                        continue;
                }

                WriteSpaces(optionsSpacesBefore);

                size_t spacesBefore = optionsSpacesBefore + strlen(opt->option) + 3;
                size_t maxWidth = (maxTerminalWidth - spacesBefore);

                printf("%s - ", opt->option);
                snprintf(buf, sizeof buf, "%s%s", opt->explanation, opt->isDefault ? " (default)" : "");
                WriteWordWrapped(buf, spacesBefore, maxWidth);

                j++;

                if (help->options[j].option)
                    printf("\n");
                else
                    break;
            }
        }
    }

    printf("\n\n");

    FreeHelpArgs();
}
