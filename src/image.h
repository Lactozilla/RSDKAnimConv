/*
 * MIT License
 *
 * Copyright (c) 2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

#include "libraries/libplum.h"

struct ImageList {
    uint8_t count;

    struct plum_image *images[UINT8_MAX];  // image data
    const char        *names[UINT8_MAX];   // image name
    char              *paths[UINT8_MAX];   // image path
};

struct plum_image *ImageLoad(const char *path);

uint64_t *ImageGetDurationMetadata(struct plum_image *image);
uint8_t *ImageGetDisposalMetadata(struct plum_image *image);

uint8_t ImageListAdd(struct ImageList *list, const char *sheet);
void ImageListFree(struct ImageList *list);

#endif
