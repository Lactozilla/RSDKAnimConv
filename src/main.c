/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "main.h"
#include "args.h"
#include "help.h"
#include "input.h"

#include "convert.h"
#include "create.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int inputFormat = FORMAT_NONE;
int outputFormat = FORMAT_NONE;

static struct ProgramArgument *supportedArgs[] = {
    NULL,
    cmdArgs_convert,
    cmdArgs_create
};

const char *GetProgramName(void)
{
    const char *programName = argList[0];

    char *pathSep = strrchr(programName, '/');

    if (!pathSep)
        pathSep = strrchr(programName, '\\');

    if (pathSep)
        programName = pathSep + 1;

    return programName;
}

int main(int argc, char **argv)
{
    argList = argv;
    argCount = argc;

    if (ARGS_USED(argCount) < 1) {
        fprintf(stderr, "usage: %s %s\n", GetProgramName(), GetUsageText());
        exit(EXIT_FAILURE);
    }

    ParseArgs(supportedArgs);

    switch (programCommand) {
        case COMMAND_CONVERT:
            ConvertSprites(inputFileNamesArg, outputFileNameArg);
            break;
        case COMMAND_CREATE:
            CreateSprites(inputFileNamesArg, outputFileNameArg);
            break;
        default:
            return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
