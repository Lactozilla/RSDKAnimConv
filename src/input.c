/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "input.h"
#include "main.h"

#include "utility/file.h"
#include "utility/path.h"
#include "utility/error.h"
#include "utility/misc.h"

#include "formats/rsdk.h"
#include "formats/json.h"

char *jsonIndentSize = NULL;
uint8_t defaultJsonIndentSize = 2;

static void SetJSONIndentationSize(uint8_t *size)
{
    if (jsonIndentSize == NULL)
        (*size) = defaultJsonIndentSize;
    else {
        int num;

        if (StringToNumber(jsonIndentSize, &num) != STRING_TO_NUMBER_OK)
            ThrowError("Invalid JSON indentation size \"%s\"", jsonIndentSize);
        else if (num > UINT8_MAX)
            ThrowError("Invalid JSON indentation size %d (maximum is %d)", num, UINT8_MAX);
        else if (num < 0)
            ThrowError("Invalid JSON indentation size %d (minimum is zero)", num);

        (*size) = (uint8_t)num;
    }
}

enum SpriteFormat GuessFormatFromFile(FILE *f)
{
    enum SpriteFormat result = FORMAT_NONE;

    // Check if it's an RSDK sprite
    if (RSDKCheckSignature(f))
        result = FORMAT_RSDK;

    // Seek back to the start, because some bytes have been read
    fseek(f, 0, SEEK_SET);

    // TODO: Check if it's a JSON file?

    return result;
}

FILE *OpenInputSprite(const char *filename)
{
    if (filename == NULL)
        ThrowError("No input file");

    FILE *f = OpenInputFile(filename);
    if (f == NULL)
        return NULL;

    if (inputFormat == FORMAT_NONE) {
        // Check the file header
        inputFormat = GuessFormatFromFile(f);

        // Couldn't guess?
        if (inputFormat == FORMAT_NONE) {
            // Guess from the filename
            int format = GuessFormatFromExtension(filename);
            if (format != FORMAT_NONE)
                inputFormat = format;
            else
                inputFormat = FORMAT_NONE;
        }
    }

    return f;
}

struct RSDKSprite *ReadSpriteFromFile(FILE *f)
{
    if (inputFormat == FORMAT_RSDK)
        return RSDKReadSprite(f);
    else if (inputFormat == FORMAT_JSON)
        return JSONReadSprite(f);
    else
        return NULL;
}

bool SaveOutputSprite(const char *filename, struct RSDKSprite *sprite)
{
    if (!filename)
        return false;

    // Autodetect output format
    if (outputFormat == FORMAT_NONE)
        outputFormat = GuessOutputFormat(inputFormat, FORMAT_RSDK, filename);

    FILE *f = OpenOutputFile(filename);
    if (f == NULL) {
        PrintError("Couldn't open output file %s\n", filename);
        return false;
    }

    if (outputFormat == FORMAT_JSON) {
        uint8_t jsonIndentSizeNum;

        SetJSONIndentationSize(&jsonIndentSizeNum);

        JSONWriteSprite(sprite, jsonIndentSizeNum, f);
    } else
        RSDKWriteSprite(sprite, f);

    CloseOutputFile();

    return true;
}
