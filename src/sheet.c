/*
 * MIT License
 *
 * Copyright (c) 2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "sheet.h"
#include "sprite.h"

#include "utility/misc.h"
#include "utility/memory.h"
#include "utility/string.h"
#include "utility/path.h"
#include "utility/error.h"

#include <stdlib.h>
#include <string.h>

static bool FindFrameBoundingBox(struct SheetFrame *frame, struct ImageList *imageList, struct BoundingBox *box);

struct SpriteSheet *SheetNew(int16_t width, int16_t height, char *path)
{
    struct SpriteSheet *sheet = MemAlloc(sizeof(*sheet));

    sheet->width = width;
    sheet->height = height;
    sheet->numFrames = 0;
    sheet->frames = NULL;
    sheet->path = path;

    return sheet;
}

void SheetDelete(struct SpriteSheet *sheet)
{
    if (sheet->frames) {
        for (size_t i = 0; i < sheet->numFrames; i++)
            free(sheet->frames[i]);
    }

    free(sheet->path);
    free(sheet->frames);
    free(sheet);
}

struct SpriteSheet *SheetCopy(struct SpriteSheet *src)
{
    struct SpriteSheet *dest = MemAlloc(sizeof(*dest));

    dest->width = src->width;
    dest->height = src->height;
    dest->numFrames = src->numFrames;
    dest->path = StringCopy(src->path);

    size_t size = dest->numFrames * sizeof(struct SheetFrame);
    dest->frames = MemAlloc(size);
    memcpy(dest->frames, src->frames, size);

    return dest;
}

struct SheetFrame *SheetAddFrame(struct SpriteSheet *sheet, struct RSDKFrame *frame,
    struct ImageList *imageList, uint8_t imageListIndex, uint32_t imgFrame, uint8_t *bgColor,
    struct BoundingBox *findBounds)
{
    if (sheet->numFrames == SIZE_MAX)
        ThrowError("Too many frames in sprite sheet");

    struct SheetFrame *sheetFrame = MemAlloc(sizeof(*sheetFrame));

    bool hasBgColor = bgColor != NULL;

    sheetFrame->x = &frame->x;
    sheetFrame->y = &frame->y;
    sheetFrame->srcX = frame->x;
    sheetFrame->srcY = frame->y;
    sheetFrame->w = frame->width;
    sheetFrame->h = frame->height;
    sheetFrame->sprSheetID = &frame->sheet;
    sheetFrame->imageListIndex = imageListIndex;
    sheetFrame->imgFrame = imgFrame;
    sheetFrame->hasBgColor = hasBgColor;
    sheetFrame->merged = NULL;

    if (hasBgColor) {
        sheetFrame->bgColor[0] = bgColor[0];
        sheetFrame->bgColor[1] = bgColor[1];
        sheetFrame->bgColor[2] = bgColor[2];
        sheetFrame->bgColor[3] = 255 - bgColor[3];
    }

    if (FindFrameBoundingBox(sheetFrame, imageList, findBounds)) {
        sheetFrame->srcX = frame->x = findBounds->x;
        sheetFrame->srcY = frame->y = findBounds->y;
        sheetFrame->w = frame->width = findBounds->w;
        sheetFrame->h = frame->height = findBounds->h;
    }

    sheet->numFrames++;
    sheet->frames = MemRealloc(sheet->frames, sheet->numFrames * sizeof(*sheet->frames));
    sheet->frames[sheet->numFrames - 1] = sheetFrame;

    return sheetFrame;
}

bool SheetCompareFrames(struct SheetFrame *frameA, struct SheetFrame *frameB, struct ImageList *imageList)
{
    struct plum_image *imageA = imageList->images[frameA->imageListIndex];
    struct plum_image *imageB = imageList->images[frameB->imageListIndex];

    uint16_t srcX[2] = { frameA->srcX, frameB->srcX };
    uint16_t srcY[2] = { frameA->srcY, frameB->srcY };
    uint16_t srcW[2] = { frameA->w, frameB->w };
    uint16_t srcH[2] = { frameA->h, frameB->h };

    if (srcW[0] != srcW[1] || srcH[0] != srcH[1])
        return false;

    uint32_t PLUM_PIXEL_ARRAY(imageDataA, imageA) = imageA->data;
    uint32_t PLUM_PIXEL_ARRAY(imageDataB, imageB) = imageB->data;

    uint32_t bgColorA = 0, bgColorB = 0;

    bool hasBgColorA = frameA->hasBgColor;
    bool hasBgColorB = frameB->hasBgColor;

    if (hasBgColorA && hasBgColorB) {
        bgColorA = PLUM_COLOR_VALUE_32(
            frameA->bgColor[0],
            frameA->bgColor[1],
            frameA->bgColor[2],
            frameA->bgColor[3]);

        bgColorB = PLUM_COLOR_VALUE_32(
            frameB->bgColor[0],
            frameB->bgColor[1],
            frameB->bgColor[2],
            frameB->bgColor[3]);
    }

    for (uint32_t y = 0; y < srcH[0]; y++) {
        for (uint32_t x = 0; x < srcW[0]; x++) {
            uint32_t pixelA = imageDataA[frameA->imgFrame][y + srcY[0]][x + srcX[0]];
            uint32_t pixelB = imageDataB[frameB->imgFrame][y + srcY[1]][x + srcX[1]];

            // Both pixels transparent
            if (hasBgColorA && hasBgColorB && pixelA == bgColorA && pixelB == bgColorB)
                continue;

            if (pixelA != pixelB)
                return false;
        }
    }

    return true;
}

void SheetRemoveFrame(struct SpriteSheet *sheet, size_t index)
{
    if (!sheet->numFrames || index >= sheet->numFrames)
        ThrowError("Can't remove frame %d", index);

    free(sheet->frames[index]);

    sheet->numFrames--;

    for (size_t i = index; i < sheet->numFrames; i++)
        sheet->frames[i] = sheet->frames[i + 1];

    if (!sheet->numFrames) {
        free(sheet->frames);
        sheet->frames = NULL;
        return;
    }

    sheet->frames = MemRealloc(sheet->frames, sheet->numFrames * sizeof(*sheet->frames));
}

#define GET_PIXEL(x, y) (imageData[frame->imgFrame][y][x])
#define PIXEL_IS_TRANSPARENT(pixel) (PLUM_ALPHA_32(pixel) == 255 || (frame->hasBgColor && pixel == bgColor))

bool FindFrameBoundingBox(struct SheetFrame *frame, struct ImageList *imageList, struct BoundingBox *box)
{
    if (!box)
        return false;

    uint16_t srcX = frame->srcX;
    uint16_t srcY = frame->srcY;
    uint16_t width = srcX + frame->w;
    uint16_t height = srcY + frame->h;

    box->x = srcX;
    box->y = srcY;
    box->w = frame->w;
    box->h = frame->h;

    if (frame->w <= 1 || frame->h <= 1)
        return false;

    struct plum_image *image = imageList->images[frame->imageListIndex];

    uint32_t PLUM_PIXEL_ARRAY(imageData, image) = image->data;

    uint16_t x, y;

    uint16_t minWidth = width;
    uint16_t maxWidth = 0;

    box->w = width;
    box->h = height;

    uint32_t bgColor = 0;
    if (frame->hasBgColor)
        bgColor = PLUM_COLOR_VALUE_32(frame->bgColor[0], frame->bgColor[1], frame->bgColor[2], frame->bgColor[3]);

    // Find min/max widths of image
    for (y = srcY; y < height; y++) {
        for (x = srcX; x < width; x++) {
            uint32_t pixel = GET_PIXEL(x, y);

            if (!PIXEL_IS_TRANSPARENT(pixel)) {
                if (x < minWidth)
                    minWidth = x;
                break;
            }
        }

        for (x = width; x > srcX; x--) {
            uint32_t pixel = GET_PIXEL(x - 1, y);

            if (!PIXEL_IS_TRANSPARENT(pixel)) {
                if (x - 1 > maxWidth)
                    maxWidth = min(x + 2, width);
                break;
            }
        }
    }

    box->x = minWidth;
    box->w = maxWidth;

    // Find first row that isn't empty
    for (y = srcY; y < height; y++) {
        for (x = minWidth; x < maxWidth; x++) {
            uint32_t pixel = GET_PIXEL(x, y);

            // Found it
            if (!PIXEL_IS_TRANSPARENT(pixel)) {
                box->y = y;
                goto findLast;
            }
        }
    }

    // It's empty?
    if (y == box->h)
        return NULL;

findLast:
    // Find last row that isn't empty
    for (y = height; y > srcY; y--) {
        for (x = maxWidth; x > minWidth; x--) {
            uint32_t pixel = GET_PIXEL(x - 1, y - 1);

            // Found it
            if (!PIXEL_IS_TRANSPARENT(pixel)) {
                box->h = min(y + 2, height);
                goto done;
            }
        }
    }

done:
    // No change
    if (box->x == srcX && box->y == srcY && box->w == width && box->h == height)
        return false;

    box->w = box->w - box->x;
    box->h = box->h - box->y;

    return true;
}

#undef GET_PIXEL
#undef PIXEL_IS_TRANSPARENT

struct PackageNode {
    struct BoundingBox box;
    struct PackageNode *right;
    struct PackageNode *bottom;
    bool used;
};

static struct PackageNode *PackageNew(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
static struct PackageNode *FindNode(struct PackageNode *root, uint16_t width, uint16_t height);
static struct BoundingBox *SplitNode(struct PackageNode *parent, uint16_t width, uint16_t height);
static struct BoundingBox *GrowNode(struct PackageNode **rootPointer, uint16_t width, uint16_t height,
    uint16_t maxWidth, uint16_t maxHeight);
static void FreeNodes(struct PackageNode *node);

static struct SpriteSheet *CopySheetForPacking(size_t frameCount, uint16_t width, uint16_t height, char *path);

static char *AddNumToSheetName(const char *filename, const char *extension, int num);
static char *IncreaseSheetNameNum(const char *filename, const char *extension);

static void ChangeGeneratedSheet(struct RSDKSprite *sprite, uint8_t index, const char *sheetPath, const char *sheetFilename);
static uint8_t AddGeneratedSheet(struct RSDKSprite *sprite, const char *sheetPath, const char *sheetFilename);

static int FrameSortFunc_WIDTH(const void *a, const void *b)
{
    const struct SheetFrame *frameA = *(const struct SheetFrame **)a;
    const struct SheetFrame *frameB = *(const struct SheetFrame **)b;

    return (frameA->w > frameB->w) - (frameA->w < frameB->w);
}

static int FrameSortFunc_HEIGHT(const void *a, const void *b)
{
    const struct SheetFrame *frameA = *(const struct SheetFrame **)a;
    const struct SheetFrame *frameB = *(const struct SheetFrame **)b;

    return (frameA->h > frameB->h) - (frameA->h < frameB->h);
}

static int FrameSortFunc_AREA(const void *a, const void *b)
{
    const struct SheetFrame *frameA = *(const struct SheetFrame **)a;
    const struct SheetFrame *frameB = *(const struct SheetFrame **)b;

    int area1 = frameA->w * frameA->h;
    int area2 = frameB->w * frameB->h;

    return (area1 > area2) - (area1 < area2);
}

static int FrameSortFunc_MAXSIDE(const void *a, const void *b)
{
    const struct SheetFrame *frameA = *(const struct SheetFrame **)a;
    const struct SheetFrame *frameB = *(const struct SheetFrame **)b;

    int side1 = max(frameA->w, frameA->h);
    int side2 = max(frameB->w, frameB->h);

    return (side1 > side2) - (side1 < side2);
}

#define SORTFUNC(type) \
static void SORT_SHEET_BY_ ## type(struct SpriteSheet *sheet) \
{ \
    qsort(sheet->frames, sheet->numFrames, sizeof(struct SheetFrame*), FrameSortFunc_ ## type); \
}

SORTFUNC(AREA)
SORTFUNC(WIDTH)
SORTFUNC(HEIGHT)
SORTFUNC(MAXSIDE)

#undef SORTFUNC

static void SortSheet(struct SpriteSheet *sheet, enum SheetSortingMode sortMode)
{
    switch (sortMode) {
        case SHEET_SORT_AREA:
            SORT_SHEET_BY_AREA(sheet);
            break;
        case SHEET_SORT_WIDTH:
            SORT_SHEET_BY_WIDTH(sheet);
            break;
        case SHEET_SORT_HEIGHT:
            SORT_SHEET_BY_HEIGHT(sheet);
            break;
        case SHEET_SORT_MAXSIDE:
            SORT_SHEET_BY_MAXSIDE(sheet);
            break;
        case SHEET_SORT_NONE:
            return;
    }

    uint16_t w = sheet->frames[0]->w;
    uint16_t h = sheet->frames[0]->h;

    sheet->width = w == 0 ? DEFAULT_SHEET_SIZE : w;
    sheet->height = h == 0 ? DEFAULT_SHEET_SIZE : h;
}

size_t SheetPackFrames(struct RSDKSprite *sprite,
    struct List *sheetList, size_t maxSheets,
    uint16_t maxWidth, uint16_t maxHeight,
    enum SheetSortingMode sortMode, struct ImageList *imageList,
    const char *sheetPath, const char *sheetFilename, const char *sheetExt)
{
    struct SpriteSheet *baseSheet = LIST_GET(sheetList, struct SpriteSheet *, 0);
#ifdef DEBUG
    if (!baseSheet)
        abort();
#endif

    // Sort all frames by size
    SortSheet(baseSheet, sortMode);

    // Add the first sheet
    const char *lastSheetFilename = sheetFilename;
    AddGeneratedSheet(sprite, sheetPath, lastSheetFilename);

    size_t totalSheets = 1;
    size_t lastFrameOffset = 0;
    size_t totalFrames = baseSheet->numFrames;

    bool *merged = MemAlloc(totalFrames * sizeof(bool));

    for (size_t i = 0; i < totalFrames; i++) {
        merged[i] = false;
        baseSheet->frames[i]->merged = &merged[i];
    }

    struct SheetFrame **frameList = MemAlloc(totalFrames * sizeof(*frameList));
    memcpy(frameList, baseSheet->frames, totalFrames * sizeof(*frameList));

    struct SpriteSheet *sheet = baseSheet;
    uint16_t defaultWidth = sheet->width;
    uint16_t defaultHeight = sheet->height;

    struct PackageNode *root = PackageNew(0, 0, defaultWidth, defaultWidth);

    for (size_t i = 0; i < totalFrames;) {
        struct SheetFrame *frame = frameList[i];

        if (frame->w == 0 || frame->h == 0)
            goto done;

        // Remove duplicated frames
        for (int j = i - 1; j >= 0; j--) {
            struct SheetFrame *lastFrame = frameList[j];

            if (!merged[j] && SheetCompareFrames(frame, lastFrame, imageList)) {
                (*frame->x) = (*lastFrame->x);
                (*frame->y) = (*lastFrame->y);
                (*frame->sprSheetID) = (*lastFrame->sprSheetID);

                merged[i] = true;

                goto done;
            }
        }

        struct PackageNode *node = FindNode(root, frame->w, frame->h);
        struct BoundingBox *where = NULL;

        if (node)
            where = SplitNode(node, frame->w, frame->h);
        else {
            where = GrowNode(&root, frame->w, frame->h, maxWidth, maxHeight);

            if (where) {
                sheet->width = root->box.w;
                sheet->height = root->box.h;
            }
        }

        // Can't fit any more frames
        if (!where) {
            size_t numFrames = i - lastFrameOffset;
            if (!numFrames || totalSheets == maxSheets)
                return 0;

            lastFrameOffset = i;

            // Make a new sheet
            char *newSheetFilename = IncreaseSheetNameNum(lastSheetFilename, sheetExt);
            lastSheetFilename = newSheetFilename;

            // Add sheet ID into sprite
            uint8_t sheetID = AddGeneratedSheet(sprite, sheetPath, newSheetFilename);

            // Create the new sheet
            struct SpriteSheet *newSheet = CopySheetForPacking(totalFrames - i, defaultWidth, defaultHeight, newSheetFilename);
            ListAdd(sheetList, newSheet);

            sheet->numFrames = numFrames;
            sheet->frames = MemRealloc(sheet->frames, sheet->numFrames * sizeof(struct SheetFrame));

            for (size_t j = 0; j < newSheet->numFrames; j++) {
                struct SheetFrame *taken = frameList[i + j];
                newSheet->frames[j] = taken;
                (*taken->sprSheetID) = sheetID;
            }

            sheet = newSheet;
            totalSheets++;

            // Sort this sheet's frames so that the root node's size
            // can fit at least the biggest frame
            SortSheet(newSheet, sortMode);
            FreeNodes(root);
            root = PackageNew(0, 0, newSheet->width, newSheet->height);
            continue;
        }

        (*frame->x) = where->x;
        (*frame->y) = where->y;

done:
        i++;
    }

    FreeNodes(root);
    free(frameList);

    // Merge similar frames
    for (size_t i = 0; i < totalSheets; i++) {
        sheet = LIST_GET(sheetList, struct SpriteSheet *, i);

        for (size_t j = 0; j < sheet->numFrames;) {
            if (*sheet->frames[j]->merged)
                SheetRemoveFrame(sheet, j);
            else
                j++;
        }
    }

    free(merged);

    // Retroactively change the first sheet's name
    if (sprite->numSheets && totalSheets > 1) {
        char *name = baseSheet->path;
        char *newName = AddNumToSheetName(name, sheetExt, 0);

        baseSheet->path = newName;
        ChangeGeneratedSheet(sprite, 0, sheetPath, newName);

        free(name);
    }

    return totalSheets;
}

struct PackageNode *PackageNew(uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{
    struct PackageNode *package = MemAlloc(sizeof(*package));

    package->box.x = x;
    package->box.y = y;
    package->box.w = w;
    package->box.h = h;

    package->right = NULL;
    package->bottom = NULL;
    package->used = false;

    return package;
}

struct PackageNode *FindNode(struct PackageNode *root, uint16_t width, uint16_t height)
{
    struct PackageNode *space = NULL;

    if (root->used) {
        space = FindNode(root->right, width, height);
        if (space)
            return space;

        space = FindNode(root->bottom, width, height);
        if (space)
            return space;
    }
    else if (width <= root->box.w && height <= root->box.h)
        return root;

    return NULL;
}

struct BoundingBox *SplitNode(struct PackageNode *node, uint16_t width, uint16_t height)
{
    node->right = PackageNew(
        node->box.x + width, node->box.y,
        node->box.w - width, height);

    node->bottom = PackageNew(
        node->box.x, node->box.y + height,
        node->box.w, node->box.h - height);

    node->used = true;

    return &node->box;
}

struct BoundingBox *GrowRootRight(struct PackageNode **rootPointer, uint16_t width, uint16_t height)
{
    struct PackageNode *root = *rootPointer;
    struct PackageNode *newRoot = PackageNew(root->box.x, root->box.y, root->box.w, root->box.h);

    *rootPointer = newRoot;

    newRoot->bottom = root;
    newRoot->right = PackageNew(root->box.w, 0, width, root->box.h);

    newRoot->used = true;
    newRoot->box.w += width;

    struct PackageNode *node = FindNode(newRoot, width, height);
    if (node)
        return SplitNode(node, width, height);

    return NULL;
}

struct BoundingBox *GrowRootDown(struct PackageNode **rootPointer, uint16_t width, uint16_t height)
{
    struct PackageNode *root = *rootPointer;
    struct PackageNode *newRoot = PackageNew(root->box.x, root->box.y, root->box.w, root->box.h);

    *rootPointer = newRoot;

    newRoot->bottom = PackageNew(0, root->box.h, root->box.w, height);
    newRoot->right = root;

    newRoot->used = true;
    newRoot->box.h += height;

    struct PackageNode *node = FindNode(newRoot, width, height);
    if (node)
        return SplitNode(node, width, height);

    return NULL;
}

struct BoundingBox *GrowNode(struct PackageNode **rootPointer, uint16_t width, uint16_t height, uint16_t maxWidth, uint16_t maxHeight)
{
    struct PackageNode *root = *rootPointer;

    uint16_t newWidth = root->box.w + width;
    uint16_t newHeight = root->box.h + height;

    bool canGrowDown = width <= root->box.w && newHeight <= maxHeight;
    bool canGrowRight = height <= root->box.h && newWidth <= maxWidth;

    if (canGrowRight && root->box.h >= newWidth)
        return GrowRootRight(rootPointer, width, height);
    else if (canGrowDown && root->box.w >= newHeight)
        return GrowRootDown(rootPointer, width, height);
    else if (canGrowRight)
        return GrowRootRight(rootPointer, width, height);
    else if (canGrowDown)
        return GrowRootDown(rootPointer, width, height);
    else
        return NULL;
}

void FreeNodes(struct PackageNode *node)
{
    if (!node)
        return;

    FreeNodes(node->right);
    FreeNodes(node->bottom);

    free(node);
}

char *AddNumToSheetName(const char *filename, const char *extension, int num)
{
    size_t fnLength = strlen(filename);
    size_t extLen = strlen(extension);

    const char *dot = FindDotInFileName(filename);
    if (dot)
        fnLength = dot - filename;

    char buf[21];
    snprintf(buf, sizeof buf, "%d", num);

    size_t bufLen = strlen(buf);

    char *out = MemAlloc(fnLength + bufLen + extLen + 1);
    memcpy(out, filename, fnLength);
    memcpy(&out[fnLength], buf, bufLen);
    memcpy(&out[fnLength + bufLen], extension, extLen + 1);

    return out;
}

char *IncreaseSheetNameNum(const char *filename, const char *extension)
{
    char *temp1, *temp2;

    size_t fnLength = strlen(filename);
    size_t extLen = strlen(extension);

    const char *dot = FindDotInFileName(filename);
    if (dot)
        fnLength = dot - filename;

    temp1 = MemAlloc(fnLength + extLen + 1);
    memcpy(temp1, filename, fnLength);
    temp1[fnLength] = '\0';

    temp2 = StringIncNum(temp1, 1, 0);
    fnLength = strlen(temp2);
    free(temp1);

    char *out = MemAlloc(fnLength + extLen + 1);
    memcpy(out, temp2, fnLength);
    memcpy(&out[fnLength], extension, extLen + 1);

    free(temp2);

    return out;
}

struct SpriteSheet *CopySheetForPacking(size_t frameCount, uint16_t width, uint16_t height, char *path)
{
    struct SpriteSheet *dest = MemAlloc(sizeof(*dest));

    dest->width = width;
    dest->height = height;
    dest->numFrames = frameCount;
    dest->path = path;
    dest->frames = MemAlloc(frameCount * sizeof(struct SheetFrame));

    return dest;
}

void ChangeGeneratedSheet(struct RSDKSprite *sprite, uint8_t index, const char *sheetPath, const char *sheetFilename)
{
    free(sprite->sheetNames[index]);
    sprite->sheetNames[index] = ConcatPathsOrDefault(sheetPath, sheetFilename, true);
}

uint8_t AddGeneratedSheet(struct RSDKSprite *sprite, const char *sheetPath, const char *sheetFilename)
{
    return SpriteAddSheet(sprite, ConcatPathsOrDefault(sheetPath, sheetFilename, true));
}
