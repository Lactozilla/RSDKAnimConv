/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "args.h"
#include "help.h"
#include "main.h"
#include "sprite.h"

#include "formats/json.h"

#include "utility/print.h"
#include "utility/error.h"
#include "utility/memory.h"
#include "utility/misc.h"

#include <stdlib.h>
#include <stddef.h>
#include <string.h>

size_t argCount = 0;
char **argList = NULL;

const char *programCommandList[] = {
    [COMMAND_NONE] = "",
    [COMMAND_CONVERT] = "convert",
    [COMMAND_CREATE] = "create"
};

size_t programCommand = COMMAND_NONE;

struct List *inputFileNamesArg;
char *outputFileNameArg;

struct OptionDict inputFormatDict[] = {
    {"rsdk", FORMAT_RSDK},
    {"json", FORMAT_JSON},

    {NULL, -1},
};

struct OptionDict outputFormatDict[] = {
    {"rsdk", FORMAT_RSDK},
    {"json", FORMAT_JSON},

    {NULL, -1},
};

struct OptionDict jsonExportModeDict[] = {
    {"hatch",          JSON_EXPORT_HATCH},
    {"rsdkanimeditor", JSON_EXPORT_RSDKANIMEDITOR},
    {"retroed2",       JSON_EXPORT_RETROED2},

    {NULL, -1},
};

static bool IsArg(const char *arg)
{
    size_t len = sizeof(ARG_DASHES) - 1;

    if (strlen(arg) < len)
        return false;

    return !memcmp(arg, ARG_DASHES, len);
}

static bool CheckArg(const char *progArg, const char *userArg)
{
    if (!IsArg(userArg))
        return false;

    if (strcmp(userArg + 2, progArg))
        return false;

    return true;
}

static bool FindArg(char *check)
{
    for (size_t i = 1; i < argCount; i++) {
        if (CheckArg(check, argList[i]))
            return true;
    }

    return false;
}

static struct ProgramArgument *FindProgArg(struct ProgramArgument *progArgs, const char *check)
{
    if (progArgs == NULL)
        return NULL;

    for (size_t i = 0; progArgs[i].name; i++) {
        if (CheckArg(progArgs[i].name, check))
            return &progArgs[i];
    }

    return NULL;
}

size_t FindProgramCommand(const char *cmd)
{
    for (size_t i = COMMAND_NONE + 1; i < NUM_PROGRAM_COMMANDS; i++) {
        if (!strcmp(cmd, programCommandList[i])) {
            return i;
        }
    }

    return 0;
}

bool SetProgramCommand(const char *cmd)
{
    size_t found = FindProgramCommand(cmd);
    if (found)
        programCommand = found;

    return found != 0;
}

void ThrowUnknownCommandError(const char *cmd)
{
    ThrowError("\"%s\" is not a known command", cmd);
}

static int CheckOptionDict(struct OptionDict *dict, const char *key)
{
    for (size_t i = 0; dict[i].key; i++) {
        if (!strcmp(dict[i].key, key))
            return i;
    }

    return -1;
}

void ParseArgs(struct ProgramArgument **supportedArgs)
{
    struct ProgramArgument *progArgList = NULL;

    bool helpArg = FindArg("help");

    for (size_t i = 1; i < argCount; i++) {
        char *arg = argList[i];

        if (CheckArg("help", arg) && i < argCount-1) {
            char *option = argList[i + 1];

            if (!IsArg(option) && !SetProgramCommand(option))
                ThrowUnknownCommandError(option);

            i++;
            progArgList = supportedArgs[programCommand];
            continue;
        }

        struct ProgramArgument *progArg = FindProgArg(progArgList, arg);
        if (progArg == NULL) {
            // Check if it's a command
            if (programCommand == COMMAND_NONE && SetProgramCommand(arg)) {
                // Worked - parse the next argument
                progArgList = supportedArgs[programCommand];
                continue;
            }

            if (helpArg)
                continue;

            if (programCommand != COMMAND_NONE || IsArg(arg))
                ThrowError("Unknown argument \"%s\"", arg);
            else
                ThrowUnknownCommandError(arg);
        }

        if (progArg->type != ARG_FLAG) {
            if (i == argCount-1) {
                // No next argument
                if (helpArg)
                    break;

                ThrowError("Argument %s is missing an option", arg);
            }

            char *option = argList[i + 1];
            if (IsArg(option)) {
                if (helpArg) {
                    i++;
                    continue;
                }

                ThrowError("Argument %s is missing an option; it was an argument instead", arg);
            }

            if (progArg->type == ARG_OPTION) {
                struct OptionDict *dict = progArg->option.dict;
                int found = CheckOptionDict(dict, option);

                if (found == -1)
                    ThrowError("Unknown option \"%s\" for argument %s", option, arg);

                if (!helpArg)
                    (*progArg->option.target) = dict[found].value;
            }
            else if (progArg->type == ARG_NUMBER) {
                int num;

                if (StringToNumber(option, &num) != STRING_TO_NUMBER_OK)
                    ThrowError("Invalid number \"%s\"", option);

                if (!helpArg)
                    (*progArg->number) = num;
            }
            else if (progArg->type == ARG_LIST) {
                size_t start = i + 1;
                size_t end = start;

                while (!IsArg(argList[end])) {
                    end++;
                    if (end == argCount)
                        break;
                }

                i = end - 1;

                size_t size = end - start;
                if (size != 0) {
                    struct List *list = ListNew(size);
                    while (start != end) {
                        ListAdd(list, argList[start]);
                        start++;
                    }

                    (*progArg->list) = list;
                }

                continue;
            }
            else if (!helpArg) // ARG_STRING
                (*progArg->string) = option;

            i++;
        } else { // ARG_FLAG
            if (i == argCount-1) {
                // No next argument
                if (helpArg)
                    break;

                (*progArg->flag) = true;
                continue;
            }

            char *option = argList[i + 1];
            if (IsArg(option)) {
                if (helpArg) {
                    i++;
                    continue;
                }

                (*progArg->flag) = true;
                continue;
            }

            if (!strcmp(option, "true") || !strcmp(option, "on") || !strcmp(option, "yes") || !strcmp(option, "1"))
                (*progArg->flag) = true;
            else if (!strcmp(option, "false") || !strcmp(option, "off") || !strcmp(option, "no") || !strcmp(option, "0"))
                (*progArg->flag) = false;
            else
                ThrowError("Invalid option \"%s\" for argument %s", option, arg);

            i++;
        }
    }

    if (helpArg) {
        ShowHelp(programCommand, progArgList);
        exit(EXIT_SUCCESS);
    }
}

struct FoundHelpArg {
    char *arg;
    int option;
};

static struct FoundHelpArg *foundHelpArgs = NULL;
static size_t numFoundHelpArgs = 0;

static void AddFoundHelpArg(char *arg, int option)
{
    numFoundHelpArgs++;
    foundHelpArgs = MemRealloc(foundHelpArgs, sizeof(*foundHelpArgs) * numFoundHelpArgs);

    struct FoundHelpArg *fh = &foundHelpArgs[numFoundHelpArgs - 1];
    fh->arg = arg;
    fh->option = option;
}

bool FindArgsForHelp(struct ProgramArgument *progArgList)
{
    size_t foundCmd = COMMAND_NONE;

    for (size_t i = 1; i < argCount; i++) {
        char *arg = argList[i];

        if (CheckArg("help", arg))
            continue;

        struct ProgramArgument *progArg = FindProgArg(progArgList, arg);
        if (progArg == NULL) {
            size_t cmd = FindProgramCommand(arg);

            if (foundCmd == COMMAND_NONE && cmd) {
                foundCmd = cmd;
                continue;
            }
            else if (foundCmd != COMMAND_NONE || IsArg(arg))
                ThrowError("Unknown argument \"%s\"", arg);
            else
                ThrowUnknownCommandError(arg);
        }

        int foundOption = -1;

        if (progArg->type == ARG_STRING || progArg->type == ARG_OPTION) {
            char *option = argList[i + 1];

            if (i == argCount-1) // No next argument
                AddFoundHelpArg(arg, foundOption);
            else if (option[0] == '-')
                AddFoundHelpArg(arg, foundOption);
            else if (progArg->type == ARG_OPTION) {
                struct OptionDict *dict = progArg->option.dict;

                int found = CheckOptionDict(dict, option);
#ifdef DEBUG
                if (found == -1) // Shouldn't happen
                    abort();
#endif

                foundOption = dict[found].value;
            }

            i++;
        }

        AddFoundHelpArg(arg, foundOption);
    }

    return numFoundHelpArgs != 0;
}

bool SearchInHelpArgs(const char *check, int *helpArgOption)
{
    if (!foundHelpArgs)
        return true;

    for (size_t i = 0; i < numFoundHelpArgs; i++) {
        if (CheckArg(check, foundHelpArgs[i].arg)) {
            *helpArgOption = foundHelpArgs[i].option;
            return true;
        }
    }

    return false;
}

bool HasHelpArgs(void)
{
    return foundHelpArgs != NULL;
}

void FreeHelpArgs(void)
{
    free(foundHelpArgs);
    foundHelpArgs = NULL;
    numFoundHelpArgs = 0;
}
