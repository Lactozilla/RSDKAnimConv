/*
 * MIT License
 *
 * Copyright (c) 2021-2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SPRITE_H
#define SPRITE_H

#include <stdint.h>
#include <stdbool.h>

#include "formats.h"
#include "formats/rsdk.h"

struct RSDKSprite *SpriteNew(void);

struct RSDKAnimation *SpriteAddAnimation(struct RSDKSprite *sprite, char *name, int16_t numFrames, bool allocFrames);
struct RSDKAnimation *SpriteSearchForAnimation(struct RSDKSprite *sprite, const char *check);

uint8_t SpriteAddSheet(struct RSDKSprite *sprite, char *sheet);
void SpriteDelete(struct RSDKSprite *sprite);

struct RSDKAnimation *AnimationNew(char *name, int16_t numFrames, uint8_t numHitboxes, bool allocFrames);
void AnimationInsertFrame(struct RSDKAnimation *anim, struct RSDKFrame *frame, int16_t index);
void AnimationRemoveFrame(struct RSDKAnimation *anim, int16_t index);
void AnimationResize(struct RSDKAnimation *anim, int16_t numFrames, uint8_t numHitboxes, bool allocFrames);
void AnimationClear(struct RSDKAnimation *anim);
void AnimationDelete(struct RSDKAnimation *anim);

struct RSDKFrame *FrameNew(uint8_t numHitboxes);
void FrameDelete(struct RSDKFrame *frame);

struct BoundingBox {
    uint32_t x, y;
    uint32_t w, h;
};

#endif
