/*
 * MIT License
 *
 * Copyright (c) 2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "create.h"

#include "main.h"
#include "args.h"
#include "input.h"
#include "sprite.h"
#include "sheet.h"
#include "image.h"

#include "utility/file.h"
#include "utility/path.h"
#include "utility/memory.h"
#include "utility/string.h"
#include "utility/print.h"
#include "utility/error.h"
#include "utility/csv.h"
#include "utility/list.h"
#include "utility/misc.h"

#include "formats/rsdk.h"
#include "formats/json.h"

#include "libraries/libplum.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define SHEET_SIZE_HARD_LIMIT 16386

static char *outputSheetPath;
static char *baseAnimName;

static bool cropFrames = true;
static bool mergeFrames = true;
static bool centerFrames = true;

static int maxSheetWidth = MAX_SHEET_SIZE;
static int maxSheetHeight = MAX_SHEET_SIZE;
static int frameSortMode = SHEET_SORT_MAXSIDE;

static char *bgColorArg;

static struct OptionDict frameSortModeDict[] = {
    {"none",    SHEET_SORT_NONE},
    {"width",   SHEET_SORT_WIDTH},
    {"height",  SHEET_SORT_HEIGHT},
    {"maxside", SHEET_SORT_MAXSIDE},
    {"area",    SHEET_SORT_AREA},

    {NULL, -1},
};

struct ProgramArgument cmdArgs_create[] = {
    COMMON_CMD_ARGS,
    COMMON_EXPORT_CMD_ARGS,

    {"outputSheetPath", ARG_STRING, .string = &outputSheetPath},
    {"animationName", ARG_STRING, .string = &baseAnimName},

    {"cropFrames", ARG_FLAG, .flag = &cropFrames},
    {"mergeFrames", ARG_FLAG, .flag = &mergeFrames},
    {"centerFrames", ARG_FLAG, .flag = &centerFrames},
    {"frameSortMode", ARG_OPTION, .option = {frameSortModeDict, &frameSortMode}},

    {"maxSheetWidth", ARG_NUMBER, .number = &maxSheetWidth},
    {"maxSheetHeight", ARG_NUMBER, .number = &maxSheetHeight},

    {"removeBackground", ARG_STRING, .string = &bgColorArg},

    {"format", ARG_OPTION, .option = {outputFormatDict, &outputFormat}},

    END_CMD_ARGS
};

static bool removeBackground;
static uint8_t backgroundColor[4] = { 255, 0, 255, 255 };

#define IS_HEX_CHAR(x) ((x >= '0' && x <= '9') || (x >= 'a' && x <= 'f') || (x >= 'A' && x <= 'F'))
#define ARE_HEX_CHARS(str, i) IS_HEX_CHAR(str[i]) && IS_HEX_CHAR(str[i + 1])

static uint32_t HexToInt(char x)
{
    if (x >= '0' && x <= '9')
        return x - '0';
    else if (x >= 'a' && x <= 'f')
        return x - 'a' + 10;
    else if (x >= 'A' && x <= 'F')
        return x - 'A' + 10;

    return 0;
}

static bool ParseBGColor(const char *str, uint8_t *color)
{
    // Parse HTML color
    if (str[0] == '#') {
        unsigned int len = strlen(++str);

        // A triplet, like #0099CC
        if (len == 6 || len == 8) {
            if (len == 6)
                color[3] = 0xFF;

            for (unsigned i = 0; i < len; i += 2) {
                if (!ARE_HEX_CHARS(str, i))
                    return false;

                *color++ = (HexToInt(str[i]) * 16) + HexToInt(str[i + 1]);
            }
        } else if (len == 3) {
            // Shorthand, like #09C
            for (unsigned i = 0; i < 3; i++) {
                if (!IS_HEX_CHAR(str[i]))
                    return false;

                uint32_t hx = HexToInt(str[i]);
                *color++ = (hx * 16) + hx;
            }

            *color = 0xFF;
        } else
            return false;
    } else {
        int red = -1, green = -1, blue = -1, alpha = -1;
        int scanned = sscanf(str, "r:%d g:%d b:%d a:%d", &red, &green, &blue, &alpha);
        if (alpha == -1)
            alpha = 255;

        if (scanned < 3
        || red < 0 || red > 255
        || green < 0 || green > 255
        || alpha < 0 || alpha > 255
        || alpha < 0 || alpha > 255)
            return false;

        color[0] = (uint8_t)red;
        color[1] = (uint8_t)green;
        color[2] = (uint8_t)blue;
        color[3] = (uint8_t)alpha;
    }

    return true;
}

#undef IS_HEX_CHAR
#undef ARE_HEX_CHARS

static int GetFrameDuration(uint64_t *durations, uint32_t frameNum)
{
    if (!durations)
        return 0;

    double finalDuration = (double)durations[frameNum] * 0.000001;

    finalDuration /= 1000.0 / 60;

    return (int)finalDuration;
}

static struct SheetFrame *AddFrame(
    struct RSDKSprite *sprite, struct RSDKAnimation *anim,
    struct AnimationFrame *srcFrame, struct SpriteSheet *sheet,
    struct ImageList *imageList, uint8_t imageListIndex,
    uint32_t frameNum, uint64_t *frameDurations,
    uint8_t *bgColor)
{
    struct BoundingBox *bbox = &srcFrame->bounds;

    if (bbox->x > UINT16_MAX)
        ThrowError("Frame X too large");
    else if (bbox->y > UINT16_MAX)
        ThrowError("Frame Y too large");
    else if (bbox->w > UINT16_MAX)
        ThrowError("Frame width too large");
    else if (bbox->h > UINT16_MAX)
        ThrowError("Frame height too large");

    struct RSDKFrame *frame = FrameNew(sprite->numHitboxes);

    frame->sheet = 0;
    frame->duration = GetFrameDuration(frameDurations, frameNum);
    frame->x = bbox->x;
    frame->y = bbox->y;
    frame->width = bbox->w;
    frame->height = bbox->h;

    if (frame->duration == 0)
        frame->duration = 1;

    if (srcFrame->isCentered) {
        frame->centerX = srcFrame->centerX;
        frame->centerY = srcFrame->centerY;
    } else if (centerFrames) {
        frame->centerX = -frame->width / 2;
        frame->centerY = -frame->height / 2;
    }

    AnimationInsertFrame(anim, frame, -1);

    struct BoundingBox box;
    struct BoundingBox *crop = cropFrames ? &box : NULL;

    struct SheetFrame *result = SheetAddFrame(sheet, frame, imageList, imageListIndex, frameNum, bgColor, crop);

    if (cropFrames) {
        frame->centerX += box.x - bbox->x;
        frame->centerY += box.y - bbox->y;
    }

    return result;
}

static bool CompareFrames(struct RSDKFrame *frameA, struct RSDKFrame *frameB)
{
    return (frameA->width == frameB->width && frameA->height == frameB->height &&
            frameA->centerX == frameB->centerX && frameA->centerY == frameB->centerY);
}

static void MergeFrames(struct RSDKAnimation *anim, struct SpriteSheet *sheet,
    struct SheetFrame **frmList, struct ImageList *imageList)
{
    int16_t numFrames = anim->numFrames - 1;

    if (anim->numFrames < 2)
        return;

    bool *merge = MemCalloc(numFrames * sizeof(bool));

    for (int16_t i = 0; i < numFrames; i++) {
        if (CompareFrames(anim->frames[i], anim->frames[i + 1])
        && SheetCompareFrames(frmList[i], frmList[i + 1], imageList))
            merge[i] = true;
    }

    for (int16_t i = 0; i < numFrames;) {
        struct RSDKFrame *frame = anim->frames[i];
        struct RSDKFrame *nextFrame = anim->frames[i + 1];

        if (merge[i]) {
            frame->duration += nextFrame->duration;

            // Remove the next frame and the sheets that point to it
            for (size_t j = 0; j < sheet->numFrames;) {
                struct SheetFrame *sheetFrame = sheet->frames[j];

                if (sheetFrame->x == &nextFrame->x
                && sheetFrame->y == &nextFrame->y
                && sheetFrame->sprSheetID == &nextFrame->sheet)
                    SheetRemoveFrame(sheet, j);
                else
                    j++;
            }

            AnimationRemoveFrame(anim, i + 1);

            for (int16_t j = i; j < numFrames - 1; j++)
                merge[j] = merge[j + 1];

            merge[numFrames - 1] = false;
            numFrames--;
        } else
            i++;
    }

    free(merge);
}

static struct plum_image *CreateOutputSheetImage(struct SpriteSheet *sheet)
{
    struct plum_image *outImage = plum_new_image();
    if (!outImage)
        return NULL;

    outImage->type = PLUM_IMAGE_PNG;
    outImage->width = sheet->width;
    outImage->height = sheet->height;
    outImage->frames = 1;
    outImage->color_format = PLUM_COLOR_32;

    uint32_t PLUM_PIXEL_ARRAY(pixels, outImage) = plum_malloc(outImage, plum_pixel_buffer_size(outImage));
    if (!pixels)
        return NULL;

    outImage->data = pixels;

    return outImage;
}

static void ClearImageData(struct plum_image *outImage, uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
    uint32_t PLUM_PIXEL_ARRAY(destData, outImage) = outImage->data;

    for (size_t iy = 0; iy < h; iy++) {
        for (size_t ix = 0; ix < w; ix++)
            destData[0][y + iy][x + ix] = PLUM_COLOR_VALUE_32(0, 0, 0, 255);
    }
}

static void CopyImageRegion(struct plum_image *outImage, struct plum_image *srcImage,
    uint16_t srcX, uint16_t srcY, uint16_t srcW, uint16_t srcH, uint32_t srcFrame,
    uint16_t destX, uint16_t destY, bool hasBgColor, uint32_t bgColor)
{
    uint32_t PLUM_PIXEL_ARRAY(destData, outImage) = outImage->data;
    uint32_t PLUM_PIXEL_ARRAY(srcData, srcImage) = srcImage->data;

    for (size_t y = 0; y < srcH; y++) {
        for (size_t x = 0; x < srcW; x++) {
            uint32_t xSrcPos = srcX + x;
            uint32_t ySrcPos = srcY + y;
            uint32_t xDestPos = destX + x;
            uint32_t yDestPos = destY + y;

            uint32_t srcPixel = srcData[srcFrame][ySrcPos][xSrcPos];

            if ((hasBgColor && srcPixel == bgColor)
            || xSrcPos >= srcImage->width
            || ySrcPos >= srcImage->height)
                continue;

            destData[0][yDestPos][xDestPos] = srcPixel;
        }
    }
}

static void CreateSheet(struct SpriteSheet *sheet, struct ImageList *imageList)
{
    struct plum_image *outImage = CreateOutputSheetImage(sheet);
    if (!outImage)
        ThrowError("Couldn't create sprite sheet image");

    ClearImageData(outImage, 0, 0, sheet->width, sheet->height);

    for (size_t i = 0; i < sheet->numFrames; i++) {
        struct SheetFrame *frame = sheet->frames[i];
        struct plum_image *srcImage = imageList->images[frame->imageListIndex];

        uint16_t srcX = frame->srcX;
        uint16_t srcY = frame->srcY;
        uint16_t srcW = frame->w;
        uint16_t srcH = frame->h;
        uint16_t destX = *frame->x;
        uint16_t destY = *frame->y;

        if (srcW == 0 || srcH == 0)
            continue;

#ifdef DEBUG
        if (destX >= sheet->width || destY >= sheet->height
        || destX + srcW - 1 >= sheet->width || destY + srcH - 1 >= sheet->height)
            abort();
#endif

        uint32_t bgColor = 0;
        bool hasBgColor = frame->hasBgColor;
        if (hasBgColor)
            bgColor = PLUM_COLOR_VALUE_32(frame->bgColor[0], frame->bgColor[1], frame->bgColor[2], frame->bgColor[3]);

        CopyImageRegion(outImage, srcImage, srcX, srcY, srcW, srcH,
            frame->imgFrame, destX, destY, hasBgColor, bgColor);
    }

    char *outSheetPath = sheet->path;

    unsigned error;
    plum_store_image(outImage, outSheetPath, PLUM_FILENAME, &error);
    plum_destroy_image(outImage);

    if (error)
        ThrowError("Couldn't save sprite sheet %s: %s\n", outSheetPath, plum_get_error_text(error));
}

static struct AnimationSource *NewAnimSource(void)
{
    return MemCalloc(sizeof(struct AnimationSource));
}

static struct AnimCollection *NewAnimCollection(void)
{
    return MemCalloc(sizeof(struct AnimCollection));
}

static struct AnimationFrame *NewAnimFrame(void)
{
    return MemCalloc(sizeof(struct AnimationFrame));
}

static void CollectionAddAnim(struct AnimCollection *collection, struct AnimationSource *source)
{
    size_t numAnims = collection->numAnimations;
    if (numAnims == SIZE_MAX)
        ThrowError("Too many animations in collection");

    collection->numAnimations++;
    collection->animations = MemRealloc(collection->animations, collection->numAnimations * sizeof(*collection->animations));

    collection->animations[numAnims] = source;
}

static struct AnimationSource *CollectionSearchForAnim(struct AnimCollection *collection, const char *check)
{
    for (size_t i = 0; i < collection->numAnimations; i++) {
        struct AnimationSource *anim = collection->animations[i];
        if (!strcmp(anim->name, check))
            return anim;
    }

    return NULL;
}

static void AnimSourceAddFrame(struct AnimationSource *source, struct AnimationFrame *frame)
{
    size_t numFrames = source->numFrames;
    if (numFrames == SIZE_MAX)
        ThrowError("Too many frames in animation");

    source->numFrames++;
    source->frames = MemRealloc(source->frames, source->numFrames * sizeof(*source->frames));

    source->frames[numFrames] = frame;
}

static void CollectionDelete(struct AnimCollection *collection)
{
    for (size_t i = 0; i < collection->numAnimations; i++) {
        struct AnimationSource *anim = collection->animations[i];

        for (size_t j = 0; j < anim->numFrames; j++) {
            struct AnimationFrame *frame = anim->frames[j];
            free(frame->sheet);
            free(frame);
        }

        free(anim->frames);
        free(anim->name);
        free(anim);
    }

    free(collection->animations);
    free(collection);
}

static void NewCollectionFromSheet(struct AnimCollection *collection,
    const char *sourceSheet, const char *animName, struct ImageList *imageList)
{
    if (!sourceSheet) {
        PrintError("No source image\n");
        return;
    }

    struct plum_image *image = imageList->images[ImageListAdd(imageList, sourceSheet)];
    if (image->frames > INT16_MAX) {
        PrintError("Image %s has too many frames -- skipping\n", sourceSheet);
        return;
    }

    struct AnimationSource *anim = NewAnimSource();

    anim->name = StringCopy(animName);
    anim->speed = 1; // 60 fps

    struct AnimationFrame *frame = NewAnimFrame();

    frame->sheet = StringCopy(sourceSheet);
    frame->bounds.x = frame->bounds.y = 0;
    frame->bounds.w = image->width;
    frame->bounds.h = image->height;
    frame->length = image->frames;
    frame->loopIndex = UINT32_MAX;
    frame->startIndex = 0;
    frame->hasBgColor = removeBackground;

    if (removeBackground)
        memcpy(frame->bgColor, backgroundColor, sizeof(uint8_t) * 4);

    AnimSourceAddFrame(anim, frame);
    CollectionAddAnim(collection, anim);
}

static void AddFrameFromSource(
    struct AnimationSource *source, struct AnimationFrame *frame,
    struct RSDKSprite *sprite, struct RSDKAnimation *sprAnim,
    struct SpriteSheet *baseSheet, struct ImageList *imageList,
    struct SheetFrame **addedSheetFrames, size_t *numAddedFrames, size_t *numAnimFrames)
{
    uint8_t imageIndex = ImageListAdd(imageList, frame->sheet);
    struct plum_image *image = imageList->images[imageIndex];

    uint32_t frameLength = frame->length;
    if (frameLength == 0)
        frameLength = 1;
    else if (frameLength > image->frames)
        frameLength = image->frames;

    uint32_t startFrame = frame->startIndex;
    uint32_t endFrame = startFrame + frameLength;

    if (endFrame > image->frames)
        endFrame = image->frames;

    AnimationResize(sprAnim, sprAnim->numFrames + frameLength, 0, false);

    uint32_t animLoopIdx;

    if (source->useLoopIndex)
        animLoopIdx = source->loopIndex;
    else {
        uint32_t loopFrame = frame->loopIndex;
        if (loopFrame == UINT32_MAX || loopFrame > image->frames)
            loopFrame = frameLength - 1;

        uint16_t lastFrameCount = (unsigned)sprAnim->numFrames;

        if (image->frames == 1)
            animLoopIdx = loopFrame;
        else if (sprAnim->loopFrame >= lastFrameCount)
            animLoopIdx = sprAnim->numFrames - 1;
        else
            animLoopIdx = (lastFrameCount - 1) + loopFrame;
    }

    sprAnim->loopFrame = animLoopIdx > UINT8_MAX ? 0 : animLoopIdx;
    sprAnim->speed = source->speed;

    uint64_t *frameDurations = ImageGetDurationMetadata(image);

    uint8_t bgColor[4] = { 0, 0, 0, 0 };
    uint8_t *bgColorPointer = NULL;

    if (frame->hasBgColor) {
        memcpy(bgColor, frame->bgColor, sizeof(uint8_t) * 4);
        bgColorPointer = bgColor;
    }

    if (mergeFrames) {
        (*numAnimFrames) += endFrame - startFrame;
        addedSheetFrames = MemRealloc(addedSheetFrames, (*numAnimFrames) * sizeof(*addedSheetFrames));
    }

    for (uint32_t frameNum = startFrame; frameNum < endFrame; frameNum++) {
        struct SheetFrame *sheetFrame = AddFrame(sprite, sprAnim, frame, baseSheet,
            imageList, imageIndex, frameNum, frameDurations, bgColorPointer);

        if (mergeFrames)
            addedSheetFrames[(*numAddedFrames)++] = sheetFrame;
    }
}

static void DoCreateSprite(
    struct RSDKSprite *sprite, struct AnimCollection *collection, struct ImageList *imageList,
    const char *sheetPath, const char *outputFn)
{
    const char *sheetExtension = ".png";
    char *newSheetFilename = ChangeFileNameExtension(outputFn, sheetExtension);

    struct SpriteSheet *baseSheet = SheetNew(DEFAULT_SHEET_SIZE, DEFAULT_SHEET_SIZE, newSheetFilename);

    struct List *sheetList = ListNew(1);
    ListAdd(sheetList, baseSheet);

    struct SheetFrame **addedSheetFrames = NULL;

    for (size_t i = 0; i < collection->numAnimations; i++) {
        struct AnimationSource *source = collection->animations[i];
        struct RSDKAnimation *sprAnim = SpriteAddAnimation(sprite, StringCopy(source->name), 0, false);
        size_t numAddedFrames = 0;
        size_t numAnimFrames = 0;

        for (size_t j = 0; j < source->numFrames; j++) {
            AddFrameFromSource(
                source, source->frames[j],
                sprite, sprAnim, baseSheet, imageList,
                addedSheetFrames, &numAddedFrames, &numAnimFrames
            );
        }

        if (addedSheetFrames)
            MergeFrames(sprAnim, baseSheet, addedSheetFrames, imageList);
    }

    free(addedSheetFrames);

    size_t sheetsMade = SheetPackFrames(sprite, sheetList, MAX_SHEET_GEN,
        (unsigned)maxSheetWidth, (unsigned)maxSheetHeight,
        (enum SheetSortingMode)frameSortMode, imageList, sheetPath, newSheetFilename, sheetExtension);

    if (!sheetsMade)
        ThrowError("Couldn't create sprite sheet; unable to fit all frames");

    for (size_t i = 0; i < sheetsMade; i++) {
        struct SpriteSheet *sheet = LIST_GET(sheetList, struct SpriteSheet *, i);
        CreateSheet(sheet, imageList);
        SheetDelete(sheet);
    }

    ListDelete(sheetList);
}

enum
{
    COLL_CSV_NAME,
    COLL_CSV_X,
    COLL_CSV_Y,
    COLL_CSV_SIZE,
    COLL_CSV_CENTER_X,
    COLL_CSV_CENTER_Y,
    COLL_CSV_SHEET,
    COLL_CSV_BACKGROUND_COLOR,
    COLL_CSV_START_FRAME,
    COLL_CSV_MAX_FRAMES,
    COLL_CSV_LOOP_FRAME,

    COLL_NUM_CSV_COLUMNS
};

const char *csvHeaderColumns[] = {
    [COLL_CSV_NAME] = "name",
    [COLL_CSV_X] = "x",
    [COLL_CSV_Y] = "y",
    [COLL_CSV_SIZE] = "size",
    [COLL_CSV_CENTER_X] = "center_x",
    [COLL_CSV_CENTER_Y] = "center_y",
    [COLL_CSV_SHEET] = "sheet",
    [COLL_CSV_BACKGROUND_COLOR] = "background_color",
    [COLL_CSV_START_FRAME] = "start_frame",
    [COLL_CSV_MAX_FRAMES] = "max_frames",
    [COLL_CSV_LOOP_FRAME] = "loop_frame"
};

static void ParseCSVNumber(const char *string, int *result, int errorLine)
{
    if (StringToNumber(string, result) != STRING_TO_NUMBER_OK)
        ThrowError("Invalid number \"%s\" at line %d", string, errorLine);
}

static void NewCollectionFromCSV(struct AnimCollection *collection, const char *sheetPath, FILE *f)
{
    // Parse column headers
    int headColIDs[COLL_NUM_CSV_COLUMNS];
    size_t numResultColumns;

    char *line = CSVReadLine(f);
    if (!line)
        ThrowError("Malformed CSV file");

    if (!CSVGetFieldNames(line, COLL_NUM_CSV_COLUMNS, headColIDs, csvHeaderColumns, &numResultColumns))
        abort();

    free(line);

    if (numResultColumns < 1)
        ThrowError("Malformed CSV header (not enough columns)");

    // Parse CSV
    unsigned int lineNum = 1;

    while ((line = CSVReadLine(f))) {
        char *column[COLL_NUM_CSV_COLUMNS];

        lineNum++;

        size_t result;

        if (!CSVGetColumns(line, column, numResultColumns, &result)) {
            if (result == 0) {
                free(line);
                continue;
            }

            ThrowError("Malformed CSV record at line %d", lineNum);
        }

#define GET_CSV_COLUMN(name, type) \
        char *name = NULL; \
        { \
            int temp = headColIDs[type]; \
            if (temp != -1) \
                name = column[temp]; \
        }

        // Read frame X and Y
        int frameX = 0, frameY = 0;

        GET_CSV_COLUMN(recFrameX, COLL_CSV_X);
        GET_CSV_COLUMN(recFrameY, COLL_CSV_Y);

        if (!StringIsEmpty(recFrameX))
            ParseCSVNumber(recFrameX, &frameX, lineNum);
        if (!StringIsEmpty(recFrameY))
            ParseCSVNumber(recFrameY, &frameY, lineNum);

        if (frameX < 0 || (unsigned)frameX > UINT32_MAX)
            ThrowError("Invalid frame X at line %d", lineNum);
        if (frameY < 0 || (unsigned)frameY > UINT32_MAX)
            ThrowError("Invalid frame Y at line %d", lineNum);

        // Read frame size
        int frameW, frameH;

        GET_CSV_COLUMN(recFrameSize, COLL_CSV_SIZE);

        if (StringIsEmpty(recFrameSize))
            ThrowError("Missing frame size at line %d", lineNum);
        else if (sscanf(recFrameSize, "%dx%d", &frameW, &frameH) != 2)
            ThrowError("Malformed frame size at line %d", lineNum);

        if (frameW < 0 || frameH < 0 || (unsigned)frameW > UINT32_MAX || (unsigned)frameH > UINT32_MAX)
            ThrowError("Invalid frame size at line %d", lineNum);

        // Read start frame, length, and loop frame
        GET_CSV_COLUMN(recStartFrame, COLL_CSV_START_FRAME);
        GET_CSV_COLUMN(recMaxFrames, COLL_CSV_MAX_FRAMES);
        GET_CSV_COLUMN(recLoopFrame, COLL_CSV_LOOP_FRAME);

        int startFrame = 0;
        int maxFrames = UINT32_MAX;
        int loopFrame = UINT32_MAX;

        if (!StringIsEmpty(recStartFrame)) {
            ParseCSVNumber(recStartFrame, &startFrame, lineNum);

            if (startFrame < 0 || (unsigned)startFrame > UINT32_MAX)
                ThrowError("Invalid start frame at line %d", lineNum);
        }

        if (!StringIsEmpty(recMaxFrames)) {
            if (strcmp(recMaxFrames, "default"))
                ParseCSVNumber(recMaxFrames, &maxFrames, lineNum);
        }

        if (!StringIsEmpty(recLoopFrame)) {
            if (strcmp(recLoopFrame, "none"))
                ParseCSVNumber(recLoopFrame, &loopFrame, lineNum);
        }

        // Read frame offsets
        GET_CSV_COLUMN(recCenterX, COLL_CSV_CENTER_X);
        GET_CSV_COLUMN(recCenterY, COLL_CSV_CENTER_Y);

        bool isCentered = false;
        int centerX = -frameW / 2;
        int centerY = -frameH / 2;

        if (!StringIsEmpty(recCenterX)) {
            if (strcmp(recCenterX, "default")) {
                ParseCSVNumber(recCenterX, &centerX, lineNum);
                isCentered = true;
            }
        }
        if (!StringIsEmpty(recCenterY)) {
            if (strcmp(recCenterY, "default")) {
                ParseCSVNumber(recCenterY, &centerY, lineNum);
                isCentered = true;
            }
        }

        // Read background color
        GET_CSV_COLUMN(recBgColor, COLL_CSV_BACKGROUND_COLOR);

        uint8_t frameBgColors[4] = {0, 0, 0, 0};
        bool hasBgColor = removeBackground;

        if (!StringIsEmpty(recBgColor)) {
            if (!strcmp(recBgColor, "default"))
                memcpy(frameBgColors, backgroundColor, sizeof(uint8_t) * 4);
            else if (!ParseBGColor(recBgColor, frameBgColors))
                ThrowError("Malformed background color at line %d", lineNum);

            hasBgColor = true;
        } else if (hasBgColor)
            memcpy(frameBgColors, backgroundColor, sizeof(uint8_t) * 4);

        // Read frame sheet
        GET_CSV_COLUMN(recFrameSheet, COLL_CSV_SHEET);

        if (StringIsEmpty(recFrameSheet))
            ThrowError("Missing sprite sheet at line %d", lineNum);

        // Read animation name
        GET_CSV_COLUMN(recAnimName, COLL_CSV_NAME);

        if (StringIsEmpty(recAnimName))
            recAnimName = baseAnimName;

        // Add into an animation source with the same name, or create one
        struct AnimationSource *anim = CollectionSearchForAnim(collection, recAnimName);

        if (anim == NULL) {
            anim = NewAnimSource();
            anim->name = StringCopy(recAnimName);
            anim->speed = 1; // 60 fps
            CollectionAddAnim(collection, anim);
        }

        struct AnimationFrame *frame = NewAnimFrame();

        frame->bounds.x = frameX;
        frame->bounds.y = frameY;
        frame->bounds.w = frameW;
        frame->bounds.h = frameH;
        frame->startIndex = (uint32_t)startFrame;
        frame->length = (uint32_t)maxFrames;
        frame->loopIndex = (uint32_t)loopFrame;
        frame->hasBgColor = hasBgColor;
        frame->sheet = ConcatPathsOrDefault(sheetPath, recFrameSheet, false);

        if (isCentered) {
            frame->centerX = centerX;
            frame->centerY = centerY;
            frame->isCentered = true;
        }

        if (hasBgColor)
            memcpy(frame->bgColor, frameBgColors, sizeof(uint8_t) * 4);

        free(line);

        AnimSourceAddFrame(anim, frame);
    }
}

static void NewCollectionFromSprite(struct AnimCollection *collection, struct RSDKSprite *sprite, const char *sheetPath)
{
    for (int16_t i = 0; i < sprite->numAnimations; i++) {
        struct RSDKAnimation *sprAnim = sprite->animations[i];
        if (!sprAnim->numFrames)
            continue;

        struct AnimationSource *src = NewAnimSource();

        src->name = StringCopy(sprAnim->name);
        src->speed = sprAnim->speed; // 60 fps

        src->useLoopIndex = true;
        src->loopIndex = sprAnim->loopFrame;

        CollectionAddAnim(collection, src);

        for (int16_t j = 0; j < sprAnim->numFrames; j++) {
            struct RSDKFrame *animFrame = sprAnim->frames[j];
            struct AnimationFrame *frame = NewAnimFrame();

            frame->bounds.x = animFrame->x;
            frame->bounds.y = animFrame->y;
            frame->bounds.w = animFrame->width;
            frame->bounds.h = animFrame->height;
            frame->centerX = animFrame->centerX;
            frame->centerY = animFrame->centerY;
            frame->isCentered = true;
            frame->length = animFrame->duration;
            frame->hasBgColor = removeBackground;
            frame->startIndex = 0;
            frame->length = 1;

            if (removeBackground)
                memcpy(frame->bgColor, backgroundColor, sizeof(uint8_t) * 4);

            frame->sheet = ConcatPathsOrDefault(sheetPath, sprite->sheetNames[animFrame->sheet], true);

            AnimSourceAddFrame(src, frame);
        }
    }
}

static void Prepare(void)
{
    if (bgColorArg) {
        if (!ParseBGColor(bgColorArg, backgroundColor))
            ThrowError("Invalid background color \"%s\"", bgColorArg);

        removeBackground = true;
    }

    if (maxSheetWidth < 1)
        ThrowError("Invalid maximum sheet width %d", maxSheetWidth);
    else if (maxSheetWidth > SHEET_SIZE_HARD_LIMIT)
        ThrowError("Invalid maximum sheet width %d (maximum is %d)", maxSheetWidth, SHEET_SIZE_HARD_LIMIT);

    if (maxSheetHeight < 1)
        ThrowError("Invalid maximum sheet height %d", maxSheetHeight);
    else if (maxSheetHeight > SHEET_SIZE_HARD_LIMIT)
        ThrowError("Invalid maximum sheet height %d (maximum is %d)", maxSheetHeight, SHEET_SIZE_HARD_LIMIT);
}

void CreateSprites(struct List *inputList, const char *outputFn)
{
    if (!inputList)
        ThrowError("No input files");

    struct AnimCollection *collection = NewAnimCollection();
    struct ImageList imageList;
    imageList.count = 0;

    size_t listSize = ListSize(inputList);
    if (listSize == 1) {
        if (StringIsEmpty(baseAnimName))
            baseAnimName = StringCopy("Untitled Animation");
        else
            baseAnimName = StringCopy(baseAnimName);
    }

    Prepare();

    for (size_t i = 0; i < listSize; i++) {
        const char *inputFn = LIST_GET(inputList, const char *, i);

        if (listSize > 1)
            baseAnimName = StringCopy(inputFn);

        if (CheckFileNameExtension(inputFn, "csv")) {
            FILE *csvFile = fopen(inputFn, "rb");
            if (csvFile == NULL) {
                PrintError("Couldn't open CSV file %s -- skipping\n", inputFn);
                continue;
            }

            char *csvPath = GetFileNamePath(inputFn);
            NewCollectionFromCSV(collection, csvPath, csvFile);
            free(csvPath);

            fclose(csvFile);
        } else {
            // Try to read a sprite
            FILE *file = OpenInputSprite(inputFn);
            if (file == NULL) {
                PrintError("Couldn't open input sprite %s -- skipping\n", inputFn);
                continue;
            }

            // Couldn't guess the input format. Must be an image, then?
            if (inputFormat == FORMAT_NONE)
                NewCollectionFromSheet(collection, inputFn, baseAnimName, &imageList);
            else {
                // It's a sprite, so import all of the animations and frames from it.
                struct RSDKSprite *sprite = ReadSpriteFromFile(file);
                if (sprite == NULL) {
                    PrintError("Couldn't read input sprite %s -- skipping\n", inputFn);
                    continue;
                }

                char *sheetPath = GetFileNamePath(inputFn);
                if (!sheetPath)
                    sheetPath = StringCopy(".");

                NewCollectionFromSprite(collection, sprite, sheetPath);

                SpriteDelete(sprite);
                free(sheetPath);
            }

            CloseInputFile();
        }

        free(baseAnimName);
        baseAnimName = NULL;
    }

    if (collection->numAnimations == 0)
        ThrowError("No animations to import");

    // Autodetect output format
    if (outputFormat == FORMAT_NONE)
        outputFormat = GuessOutputFormat(inputFormat, FORMAT_RSDK, outputFn);

    // Make the output filename
    char *outFileName;
    if (listSize > 1 && !outputFn)
        outFileName = MakeOutputSpriteFileName(outputFn, "output", outputFormat);
    else
        outFileName = MakeOutputSpriteFileName(outputFn, ListGet(inputList, 0), outputFormat);

    // Create the sprite and its sheets
    struct RSDKSprite *sprite = SpriteNew();

    DoCreateSprite(sprite, collection, &imageList, outputSheetPath, outFileName);
    CollectionDelete(collection);
    ImageListFree(&imageList);

    if (!SaveOutputSprite(outFileName, sprite))
        ThrowError("Couldn't write output sprite");

    SpriteDelete(sprite);
    free(outFileName);
}
