# MIT License
#
# Copyright (c) 2021-2022 Jaime Ita Passos
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

PROGRAM_NAME := animconv
BUILD_TYPE   := release

SOURCE_DIR := src
OBJECT_DIR := obj
OUTPUT_DIR := bin

OBJECT_DIR_PARENT := $(OBJECT_DIR)
OUTPUT_DIR_PARENT := $(OUTPUT_DIR)

ifeq ($(MAKECMDGOALS),release)
	BUILD_TYPE = release
else ifeq ($(MAKECMDGOALS),debug)
	BUILD_TYPE = debug
endif

ifneq ($(MAKECMDGOALS),clean)
	OBJECT_DIR := $(OBJECT_DIR)/$(BUILD_TYPE)
	OUTPUT_DIR := $(OUTPUT_DIR)/$(BUILD_TYPE)
endif

PROGRAM_SUBDIRS := utility formats libraries

PROGRAM_SOURCES := \
	main.c input.c args.c help.c \
	convert.c create.c \
	sprite.c sheet.c image.c \
	utility/memory.c utility/string.c utility/list.c \
	utility/file.c utility/reader.c utility/writer.c \
	utility/path.c utility/parser.c utility/csv.c \
	utility/print.c utility/error.c utility/misc.c \
	formats/rsdk.c formats/json.c

LIBRARY_SOURCES := libraries/libplum.c

PROGRAM_DEFINES := \
	TESTS_ENABLED FILE_DEBUG \
	RSDK_READER_DEBUG \
	JSON_PARSER_DEBUG JSON_IMPORTER_DEBUG

OUTPUT_FILE  := $(OUTPUT_DIR)/$(PROGRAM_NAME)
SOURCE_FILES := $(foreach file, $(PROGRAM_SOURCES), $(SOURCE_DIR)/$(file))
OBJECT_FILES := $(SOURCE_FILES:$(SOURCE_DIR)/%.c=$(OBJECT_DIR)/%.o)

LIBSRC_FILES := $(foreach file, $(LIBRARY_SOURCES), $(SOURCE_DIR)/$(file))
LIBOBJ_FILES := $(LIBSRC_FILES:$(SOURCE_DIR)/%.c=$(OBJECT_DIR)/%.o)

ALL_SUBDIRS = $(OUTPUT_DIR) $(foreach dir,$(PROGRAM_SUBDIRS),$(OBJECT_DIR)/$(dir))

CPPFLAGS := -MMD -MP
CFLAGS   := -Wall -Wextra $(foreach def,$(PROGRAM_DEFINES),$(if $($(def)),-D$(def)))

ifeq ($(BUILD_TYPE),debug)
	CFLAGS += -g -DTESTS_ENABLED -DDEBUG
endif

.PHONY: all release debug prep clean

all: prep $(OUTPUT_FILE)

release: all
debug: all

prep:
	@$(foreach dir,$(ALL_SUBDIRS),mkdir -p $(dir) ;)

# Build the executable
$(OUTPUT_DIR):
$(OUTPUT_FILE): $(OBJECT_FILES) $(LIBOBJ_FILES) | $(OUTPUT_DIR)
	@$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@

# Compile source files
$(OBJECT_DIR):
$(OBJECT_DIR)/%.o: $(SOURCE_DIR)/%.c | $(OBJECT_DIR)
	@$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

# Compile libraries
$(OBJECT_DIR)/libraries/%.o: $(SOURCE_DIR)/libraries/%.c
	@$(CC) -c $< -o $@

clean:
	@$(RM) -r $(OBJECT_DIR_PARENT) $(OUTPUT_DIR_PARENT)

-include $(OBJECT_FILES:.o=.d)
