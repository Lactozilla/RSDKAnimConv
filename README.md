# RSDKAnimConv (RSDK Animation Converter)

RSDKAnimConv is a tool that can convert RSDKv5 animations to other formats, such as JSON, and even back into the RSDKv5 format. It requires no dependencies, and is written in C99.

Run with `--help` for usage info.

This software is licensed under the MIT License.
